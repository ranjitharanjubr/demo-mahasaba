import Batch from "./pages/Batch/Batch";
import BatchCount from "./pages/Batch/BatchCount";
import Payments from "./pages/Payments";
import User from "./pages/User";
import AdsMenu from "./pages/Advertisement/AdsMenu";
import News from "./pages/News";
import SuccessStory from "./pages/SuccessStory";
import Notification from "./pages/Notification";
import Settings from "./pages/Settings";
import Timetable from "./pages/Timetable";
import Course from "./pages/Course/Course";
import StoreBook from "./pages/StoreBook/StoreBook";
import Masters from "./pages/Masters/Masters";
import Category from "./pages/Masters/Category";
import Discount from "./pages/Masters/Discount";
import Language from "./pages/Masters/Language";
import Region from "./pages/Masters/Region";
import Promo from "./pages/Advertisement/Promo";
import Advertisement from "./pages/Advertisement/Advertisement";
import CourseMenu from "./pages/Course/CourseMenu";
import Module from "./pages/Course/Module";
import Topic from "./pages/Course/Topic";
import Qualification from "./pages/Masters/Qualification";
import Designation from "./pages/Masters/Designation";
import Department from "./pages/Masters/Department";
import Members from "./pages/Member";
import Membership from "./components/Membership";
import Directors from "./components/Directors";
import Expenditure from "./components/Expenditure";

const Routings = [
  {
    path: "/Admin/batch",
    name: "batch",
    element: <Batch />,
  },
  {
    path:"/Admin/batchcount",
    name:"batch",
    element:<BatchCount/>
  },
  {
    path: "/Admin/course",
    name: "course",
    element: <Course />,
  },
  {
    path: "/Admin/storebook",
    name: "store", 
    element: <StoreBook />,
  },
  {
    path: "/Admin/masters",
    name: "masters",
    element: <Masters />,
  },
  {
    path: "/Admin/adsmenu",
    name: "adsmenu",
    element: <AdsMenu />,
  },
  {
    path: "/Admin/successstory",
    name: "successstory",
    element: <SuccessStory />,
  },
  {
    path: "/Admin/news",
    name: "news",
    element: <News />,
  },
  {
    path: "/Admin/user",
    name: "user",
    element: <User />,
  },
  {
    path: "/Admin/payments",
    name: "payments",
    element: <Payments />,
  },
  {
    path: "/Admin/notification",
    name: "notification",
    element: <Notification />,
  },
  {
    path: "/Admin/timetable",
    name: "timetable",
    element: <Timetable />,
  },
  {
    path: "/Admin/settings",
    name: "settings",
    element: <Settings />,
  },
  {
    path: "/Admin/category",
    name: "category",
    element: <Category />,
  },
  {
    path: "/Admin/discount",
    name: "discount",
    element: <Discount />,
  },
  {
    path: "/Admin/language",
    name: "language",
    element: <Language />,
  },
  {
    path: "/Admin/region",
    name: "region",
    element: <Region />,
  },
  {
    path: "/Admin/advertisement",
    name: "advertisement",
    element: <Advertisement />,
  },
  {
    path: "/Admin/promo",
    name: "promo",
    element: <Promo />,
  },
  {
    path: "/Admin/course-menu",
    name: "course-menu",
    element: <CourseMenu />,
  },
  {
    path: "/Admin/module",
    name: "module",
    element: <Module />,
  },
  {
    path: "/Admin/topic",
    name: "topic",
    element: <Topic />,
  },
  {
    path: "/Admin/Qualification",
    name: "Qualification",
    element: <Qualification />,
  },
  {
    path: "/Admin/Designation",
    name: "Designation",
    element: <Designation />,
  },
  {
    path: "/Admin/Department",
    name: "Department",
    element: <Department />,
  },
  {
    path: "/Admin/Members",
    name: "Members",
    element: <Members />,
  },
  {
    path: "/Admin/Membership",
    name: "Membership",
    element: <Membership />,
  },
  {
    path: "/Admin/Directors",
    name: "Directors",
    element: <Directors />,
  },
  {
    path: "/Admin/Expenditure",
    name: "Expenditure",
    element: <Expenditure />,
  },
];

export default Routings;
