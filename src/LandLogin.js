import React from "react";
import logo from "./assets/images/logo.png";
import AuthModal from "./components/Auth/AuthModal";

export default function LandLogin() {
  const date = new Date();
  const year = date.getFullYear();
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <>
      <div>
        <section>
          <header
            id="header"
            className="header-land fixed-top"
            style={{ backgroundColor: "#fff" }}
          >
            <div className="container-fluid container-xl d-flex align-items-center justify-content-between">
              <a href="index.html" className="logo d-flex align-items-center">
                <img src={logo} alt="" />
                <span>Mahaasabha</span>
              </a>

              <nav id="navbar" className="navbar">
                <ul>
                  <li>
                    <a
                      className=" getstarted btn-get-started scrollto d-inline-flex align-items-center justify-content-center align-self-center"
                      onClick={() => setModalShow(true)}
                    >
                      <span>Login</span>
                      <i className="bi bi-arrow-right"></i>
                    </a>
                  </li>
                </ul>
                <i className="bi bi-list mobile-nav-toggle"></i>
              </nav>
            </div>
          </header>
        </section>
        <div>
          <AuthModal show={modalShow} onHide={(e) => setModalShow(false)} />
        </div>
      </div>
      <div>
        <section id="hero" className="hero-land d-flex align-items-center">
          <div className="container">
            <div className="row">
              <div className="col-lg-6 d-flex flex-column justify-content-center">
                <h1>Welcome to Mahaasabha</h1>
                <h2>
                  On Mahaasabha you can connect with people of your community ,
                  make connections within your community, meet new people of
                  your community, make friends from within your community.
                </h2>
                <div>
                  <div className="text-center text-lg-start d-inline-flex align-items-center justify-content-center align-self-center mt-4">
                    <a className="btn-get-started d-inline-flex align-items-center justify-content-center align-self-center m-2">
                      {/* <span>Get Started</span>
                    <i className="bi bi-arrow-right"></i> */}
                      <span>Web</span>
                      <i class="fas fa-globe"></i>
                    </a>
                    <a className="btn-get-started d-inline-flex align-items-center justify-content-center align-self-center m-2">
                      {/* <span>Get Started</span>
                    <i className="bi bi-arrow-right"></i> */}
                      <span>Android</span>
                      <i class="fab fa-android"></i>
                    </a>
                    <a className="btn-get-started d-inline-flex align-items-center justify-content-center align-self-center m-2">
                      {/* <span>Get Started</span>
                    <i className="bi bi-arrow-right"></i> */}
                      <span>iOS</span>
                      <i class="fab fa-apple"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 hero-img">
                <img
                  src="assets/img/hero-img.png"
                  className="img-fluid"
                  alt=""
                />
              </div>
            </div>
          </div>
        </section>
      </div>
      <div>
        <section>
          <footer className="footer-land">
            <div className="footer-top">
              <div className="container">
                <div className="row gy-4">
                  <div className="col-lg-6 col-md-12 footer-info">
                    <a href="" className="logo d-flex align-items-center">
                      <img src={logo} alt="" />
                      <span>BitStreamIO</span>
                    </a>
                    <p>
                      BitStreamio gives you the best career growth platforms
                      that help to build the goal-oriented for a better future.
                      Since 2019, we are giving better software training and
                      technical solutions the world for effectively dealing with
                      dynamic websites such as web applications, AL/ML/DL, SEO,
                      Mobile Apps (Android, IoS, Hybrid), many more.
                    </p>
                    <div className="social-links mt-3">
                      <a href="" className="twitter">
                        <i className="bi bi-twitter"></i>
                      </a>
                      <a href="" className="facebook">
                        <i className="bi bi-facebook"></i>
                      </a>
                      <a href="" className="instagram">
                        <i className="bi bi-instagram"></i>
                      </a>
                      <a href="" className="linkedin">
                        <i className="bi bi-linkedin"></i>
                      </a>
                    </div>
                  </div>

                  <div className="col-lg-6 col-md-12 footer-contact text-center ">
                    <h4>Contact Us</h4>
                    <p>
                      385/38 2nd Floor, 12th Main Rd, 6th Block, Rajajinagar,
                      Bengaluru, Karnataka 560010
                    </p>
                    <p>Phone: +91 7975455855.</p>
                    <p>Email: info@bitstreamio.com</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="copyright">
              &copy; {year} Copyright{" "}
              <strong>
                <span>BitStreamIO</span>
              </strong>
              . All Rights Reserved
            </div>
            <div class="credits">
              Designed by{" "}
              <a href="https://walkinsoftwares.com/" target="blank">
                Walkin Software Technologies
              </a>
            </div>
          </footer>
        </section>
      </div>
    </>
  );
}
