import React, { useState, useEffect } from "react";
import { Card } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import BaseUrl from "../BaseUrl";
import userimg from "../assets/images/userimg.png";

export default function User() {
  const [userInfo, setUserInfo] = useState({});
  const [email, setEmail] = useState("");
  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

  const fetchUser = () => {
    axios({
      method: "get",
      url: `${BaseUrl}/login/v1/queryUserProfileByUserName/${user.userName}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setUserInfo(data);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchUser();
  }, []);


  let navigate = useNavigate();
  return (
    <>
      <main id="main" className="main1">
        <div className="pagetitle">
          <h1>My Account</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/Admin">Home</Link>
              </li>
              <li className="breadcrumb-item active">My Account</li>
            </ol>
          </nav>
        </div>
        {/* <div className="row">
        
        </div> */}
        <section class="section profile">
          <div class="row">
          <div className="col-md-4"> <div class="col-xl-8">
              <div class="card">
                <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">
                  <img src={userimg} alt="Profile" class="rounded-circle" />
                  <h2>{userInfo.userName}</h2>
                  <h3>{userInfo.mobileNumber}</h3>
                  <h3>{userInfo.email}</h3>
                  <div class="social-links mt-2">
                    <a href="#" class="twitter">
                      <i class="bi bi-twitter"></i>
                    </a>
                    <a href="#" class="facebook">
                      <i class="bi bi-facebook"></i>
                    </a>
                    <a href="#" class="instagram">
                      <i class="bi bi-instagram"></i>
                    </a>
                    <a href="#" class="linkedin">
                      <i class="bi bi-linkedin"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div></div>
          <div className="col-md-8"> <div class="col-xxl-8">
              <div class="card">
                <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">
                  {/* <img src={userimg} alt="Profile" class="rounded-circle" /> */}

<div className="ul">
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Overview


</a></li>
    <li><a data-toggle="tab" href="#menu1">Edit Profile</a></li>
    <li><a data-toggle="tab" href="#menu2">Settings</a></li>
    <li><a data-toggle="tab" href="#menu3">Change Password</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      
      <div  >
                  <h5 class="card-title">About</h5>
                  <p class="small fst-italic">Sunt est soluta temporibus accusantium neque nam maiores cumque temporibus. Tempora libero non est unde veniam est qui dolor. Ut sunt iure rerum quae quisquam autem eveniet perspiciatis odit. Fuga sequi sed ea saepe at unde.</p>

                  <h5 class="card-title">Profile Details</h5>

                  <div class="row">
                    <div class="col-lg-3 col-md-4  ">Full Name</div>
                    <div class="col-lg-9 col-md-8">{userInfo.userName}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 ">mobileNumber</div>
                    <div class="col-lg-9 col-md-8">{userInfo.mobileNumber}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 ">Email</div>
                    <div class="col-lg-9 col-md-8">{userInfo.email}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 ">password</div>
                    <div class="col-lg-9 col-md-8">{userInfo.password}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 ">newPassword</div>
                    <div class="col-lg-9 col-md-8">{userInfo.newPassword}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 ">otp</div>
                    <div class="col-lg-9 col-md-8">{userInfo.otp}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 ">imeiNumber</div>
                    <div class="col-lg-9 col-md-8">{userInfo.imeiNumber}</div>
                  </div>
                  <div class="row">
                    <div class="col-lg-3 col-md-4 ">insertedDate</div>
                    <div class="col-lg-9 col-md-8">{userInfo.insertedDate}</div>
                  </div>
                  <div class="row">
                    <div class="col-lg-3 col-md-4 ">updatedDate</div>
                    <div class="col-lg-9 col-md-8">{userInfo.updatedDate}</div>
                  </div>
                  <div class="row">
                    <div class="col-lg-3 col-md-4 ">role</div>
                    <div class="col-lg-9 col-md-8">{userInfo.role}</div>
                  </div>
                  <div class="row">
                    <div class="col-lg-3 col-md-4 ">roleDto</div>
                    <div class="col-lg-9 col-md-8">{userInfo.roleDto}</div>
                  </div>

                </div>
    </div>
    <div id="menu1" class="tab-pane fade">
      <h3>Edit Profile</h3>
      <div class="row mb-3">
                      <label for="profileImage" class="col-md-4 col-lg-3 col-form-label">Profile Image</label>
                      <div class="col-md-8 col-lg-9">
                        <img src="assets/img/profile-img.jpg" alt="Profile"/>
                        <div class="pt-2">
                          <a href="#" class="btn btn-primary btn-sm" title="Upload new profile image"><i class="bi bi-upload"></i></a>
                          <a href="#" class="btn btn-danger btn-sm" title="Remove my profile image"><i class="bi bi-trash"></i></a>
                        </div>
                      </div>
                    </div>
    </div>
    <div id="menu2" class="tab-pane fade">
      <h3>Setting</h3>
      <div class="row mb-3">
                      <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Email Notifications</label>
                      <div class="col-md-8 col-lg-9">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="changesMade" checked/>
                          <label class="form-check-label" for="changesMade">
                            Changes made to your account
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="newProducts" checked/>
                          <label class="form-check-label" for="newProducts">
                            Information on new products and services
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="proOffers"/>
                          <label class="form-check-label" for="proOffers">
                            Marketing and promo offers
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="securityNotify" checked disabled/>
                          <label class="form-check-label" for="securityNotify">
                            Security alerts
                          </label>
                        </div>
                      </div>
                    </div>

                    <div class="text-center">
                      <button type="submit" class="btn btn-primary">Save Changes</button>
                    </div>
  </div>
  <div id="menu3" class="tab-pane fade">
      <h3>Change Password</h3>
      <div class="row mb-3">
                      <label for="currentPassword" class="col-md-4 col-lg-3 col-form-label">Current Password</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="password" type="password" class="form-control" id="currentPassword"/>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="newPassword" class="col-md-4 col-lg-3 col-form-label">New Password</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="newpassword" type="password" class="form-control" id="newPassword"/>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="renewPassword" class="col-md-4 col-lg-3 col-form-label">Re-enter New Password</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="renewpassword" type="password" class="form-control" id="renewPassword"/>
                      </div>
                    </div>

                    <div class="text-center">
                      <button type="submit" class="btn btn-primary">Change Password</button>
                    </div>
  </div>

                  </div>
                  <h2>{userInfo.userName}</h2>
                  <h3>{userInfo.mobileNumber}</h3>
                  <h3>{userInfo.email}</h3>
                  {/* <div class="social-links mt-2">
                    <a href="#" class="twitter">
                      <i class="bi bi-twitter"></i>
                    </a>
                    <a href="#" class="facebook">
                      <i class="bi bi-facebook"></i>
                    </a>
                    <a href="#" class="instagram">
                      <i class="bi bi-instagram"></i>
                    </a>
                    <a href="#" class="linkedin">
                      <i class="bi bi-linkedin"></i>
                    </a>
                  </div> */}
                </div>
              </div>
            </div></div>
           
          </div>
        
      
      
       </div>
       
       
        </section>
       
      </main>
    </>
  );
}
