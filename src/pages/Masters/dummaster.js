import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import BaseUrl from "../../BaseUrl";

export default function Masters() {
  const [countData, setCountData] = useState([]);

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

  const fetchCountData = async () => {
    await axios({
      method: "get",
      url: `${BaseUrl}/dashboard/v1/masterTableDashboard`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setCountData(data);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchCountData();
  }, []);

  let navigate = useNavigate();

  return (
    <>
      <main id="main" className="main" style={{ display: "block" }}>
        <div className="pagetitle mb-5">
          <h1>Masters</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/Admin">Home</Link>
              </li>
              <li className="breadcrumb-item active">Masters</li>
            </ol>
          </nav>
        </div>

        <section class="section dashboard">
          <div class="col-12">
            <div class="row">
              <div
                class="col-3 "
                onClick={(e) => navigate("/Admin/Qualification")}
              >
               <div class="card info-card customers-card">
                  <div class="card-body">
                    <h5 class="card-title">Qualification</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-translate"></i>
                      </div>
                      <div class="ps-3">
                        <h6>{countData.languageCount}</h6>
                      </div>
                    </div>
                  </div>
                </div>
               
              </div>
              <div
                class="col-3 "
                onClick={(e) => navigate("/Admin/Designation")}
              >
                <div class="card info-card language-card">
                  <div class="card-body">
                    <h5 class="card-title">Designation</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-map"></i>
                      </div>
                      <div class="ps-3">
                        <h6>0</h6>
                      </div>
                    </div>
                  </div>
                </div>

                {/* <div class="card info-card customers-card">
                  <div class="card-body">
                    <h5 class="card-title">Financial Year</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-translate"></i>
                      </div>
                      <div class="ps-3">
                        <h6>{countData.languageCount}</h6>
                      </div>
                    </div>
                  </div>
                </div> */}
              </div>

              <div
                class="col-3"
                onClick={(e) => navigate("/Admin/Category")}
              >
             
             <div class="card info-card language-card">
                  <div class="card-body">
                    <h5 class="card-title">Category</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-map"></i>
                      </div>
                      <div class="ps-3">
                        <h6>0</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div
                class="col-3 "
                onClick={(e) => navigate("/Admin/Department")}
              >
                <div class="card info-card language-card">
                  <div class="card-body">
                    <h5 class="card-title">Department</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-map"></i>
                      </div>
                      <div class="ps-3">
                        <h6>0</h6>
                      </div>
                    </div>
                  </div>
                </div>

                {/* <div class="card info-card customers-card">
                  <div class="card-body">
                    <h5 class="card-title">Financial Year</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-translate"></i>
                      </div>
                      <div class="ps-3">
                        <h6>{countData.languageCount}</h6>
                      </div>
                    </div>
                  </div>
                </div> */}
              </div>
              <div
                class="col-3 "
                onClick={(e) => navigate("/Admin/region")}
              >
              
              </div>
            </div>
          </div>
        </section>
      </main>
    </>
  );
}
