import React, { useState, useEffect } from "react";
import { Table, Form, Button, Tabs, Tab } from "react-bootstrap";
import Moment from "moment";
import axios from "axios";
import { Link, useNavigate } from "react-router-dom";
import { Card } from "react-bootstrap";
// import BaseUrl from "../../BaseUrl";
import ReactPaginate from "react-paginate";
import { toast } from "react-toastify";


export default function Qualification() {
    const navigate=useNavigate();
  const [key, setKey] = useState("list-view");
  const [qualificationName, setQualificationName] = useState("");
  const [qualification, setQualification] = useState([]);
  const [description, setDescription] = useState("");
  const [onEdit, setOnEdit] = useState(false);
  const [qualificationId, setQualificationId] = useState("");
  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };
  const baseUrl="https://executivetracking.cloudjiffy.net/Mahaasabha"

  const postData = (e) => {
    e.preventDefault();
    console.log(qualificationName, description);
    axios({
      method: "post",
      url: `${baseUrl}/qualification/v1/createQualification`,
      headers,
      data: {
        createdBy: {
          userId: user.userId,
        },
        qualificationName,
        description,
      },
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
        //   alert(res.data.message);
        toast.success(res.data.message)
         window.location.reload();
       // navigate("/Admin/Designation");
        } else if (res.data.responseCode === 400) {
          toast.error(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setQualificationName("");
    setDescription("");
    setKey("list-view");
    FetchData();
  };

  const FetchData = async () => {
    await axios({
      method: "get",
      url: `${baseUrl}/qualification/v1/getAllQualificationByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers, 
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setQualification(data.content);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    FetchData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    FetchData(pageSize);
  };

  useEffect(() => {
    FetchData();
  }, [qualificationName, description, pageSize, pageNumber]);

  const handleRemove = (qualificationId) => {
    axios({
      method: "delete",
      url: `${baseUrl}/qualification/v1/deleteQualificationById/${qualificationId}`,
      headers,    
    })
      .then((res) => {
        if (res.data.responseCode === 200) {
          //alert(res.data.message);
          toast.success(res.data.message)
        } else if (res.data.responseCode === 400) {
          toast.error(res.data.errorMessage);
        }
        FetchData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleEdit = (qualificationId) => {
    axios({
      method: "get",
      url: `https://executivetracking.cloudjiffy.net/Mahaasabha/qualification/v1/getQualificationByQualificationId/${qualificationId}`,
      
      headers,  
    })
      .then(function (res) {
        setQualificationName(res.data.qualificationName);
        setDescription(res.data.description);
        setOnEdit(true);
        setKey("add");
        setQualificationId(qualificationId);
        FetchData();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const editData = (e) => {
    e.preventDefault();
    axios({
      method: "put",
      url: `${baseUrl}/qualification/v1/updateQualification`,
      https:
      headers,
      data: {
        description,
        qualificationId,
        qualificationName,
        updatedBy: {
          userId: user.userId,
        },
      },
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
         //alert(res.data.message);
          toast.success(res.data.message)
        } else if (res.data.responseCode === 400) {
          toast.error(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setQualificationName("");
    setDescription("");
    setOnEdit(false);
    setKey("list-view");
    FetchData();
  };

  return (
    <>
      <main id="main" className="main">
        <div className={key === "add" ? "pagetitle mb-5" : "pagetitle"}>
          <h1>Qualification</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/Admin/masters">Masters</Link>
              </li>
              <li className="breadcrumb-item active">Qualification</li>
            </ol>
          </nav>
        </div>
        <div className={key === "list-view" ? "displayForm mt-3" : "hideForm"}>
          <select
            value={value}
            onChange={handleChange}
            style={{ borderColor: "powderblue", borderRadius: 3, padding: 2 }}
          >
            <option value="10">10 / Pages</option>
            <option value="25">25 / Pages</option>
            <option value="50">50 / Pages</option>
            <option value="100">100 / Pages</option>
          </select>
        </div>
        <Card style={{ marginTop: 10 }}>
          <Card.Body>
            <Tabs
              activeKey={key}
              onSelect={(key) => setKey(key)}
              className="mt-3"
            >
              <Tab eventKey="list-view" title="List View">
                <Table striped bordered hover responsive>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Qualification Name</th>
                      <th>Description</th>
                      <th>Created By</th>
                      <th>Updated By</th>
                      <th>Inserted Date</th>
                      <th>Updated Date</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {qualification.map((data, index) => {
                      return (
                        <tr key={data.qualificationId}>
                          <th scope="row">{index + 1}</th>
                          <td>{data.qualificationName}</td>
                          <td
                            style={{
                              "max-width": "200px",
                              overflow: "hidden",
                              "text-overflow": "ellipsis",
                              height: "56px",
                            }}
                          >
                            {data.description}
                          </td>
                          <td>{data.createdBy.userName}</td>
                          <td>{data.updatedBy.userName}</td>
                          <td>
                            {Moment(data.insertedDate).format("DD-MM-YYYY")}
                          </td>
                          <td>
                            {Moment(data.updatedDate).format("DD-MM-YYYY")}
                          </td>
                          <td>
                            <button
                              type="button"
                              className="btn btn-icon btn-sm"
                              title="Edit"
                              onClick={(e) => handleEdit(data.qualificationId, e)}
                            //   onClick={console.log(data.designationId)}
                            >
                              <i class="bi bi-pencil-square"></i>
                            </button>
                            <button
                              type="button"
                              className="btn btn-icon btn-sm js-sweetalert"
                              title="Delete"
                              onClick={(e) => handleRemove(data.qualificationId, e)}
                            >
                              <i class="bi bi-trash"></i>
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
                <div>
                  <ReactPaginate
                    previousLabel="<"
                    nextLabel=">"
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination justify-content-center"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              </Tab>
              <Tab eventKey="add" title={onEdit ? "Edit" : "Add"}>
                <Form>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Qualification Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Qualification Name"
                      value={qualificationName}
                      onChange={(e) => setQualificationName(e.target.value)}
                    />
                    {/* <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text> */}
                  </Form.Group>

                  <Form.Group className="mb-3 col-12">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add Description"
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                    />
                  </Form.Group>
                  {onEdit ? (
                    <Button
                      variant="primary"
                      onClick={(e) => editData(e)}
                      type="submit"
                    >
                      Edit
                    </Button>
                  ) : (
                    <Button
                      variant="primary"
                      onClick={(e) => postData(e)}
                      type="submit"
                    >
                      Submit
                    </Button>
                  )}
                  <Button
                    variant="outline-primary"
                    type="submit"
                    style={{ marginLeft: 15 }}
                    onClick={(e) => setKey(1)}
                  >
                    Cancel
                  </Button>
                </Form>
              </Tab>
            </Tabs>
          </Card.Body>
        </Card>
      </main>
    </>
  );
}
