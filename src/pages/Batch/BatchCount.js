import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import BaseUrl from "../../BaseUrl";

export default function Masters() {
  const [countData, setCountData] = useState([]);

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

  const fetchCountData = async () => {
    await axios({
      method: "get",
      url: `${BaseUrl}/dashboard/v1/batchDashboard`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setCountData(data);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchCountData();
  }, []);

  let navigate = useNavigate();

  return (
    <>
      <main id="main" className="main" style={{ display: "block" }}>
        <div className="pagetitle mb-5">
          <h1>Batch</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/Admin">Home</Link>
              </li>
              <li className="breadcrumb-item active">Batch</li>
            </ol>
          </nav>
        </div>

        <section class="section dashboard">
          <div class="col-lg-12">
            <div class="row">
              <div
                class="col-xxl-4 col-md-2"
                onClick={(e) => navigate("/Admin/batch")}
              >
                <div class="card info-card sales-card">
                  <div class="card-body">
                    <h5 class="card-title">Batch</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-list-columns"></i>
                      </div>
                      <div class="ps-3">
                        <h6>{countData.batchCount}</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
             
              
              
            </div>
          </div>
        </section>
      </main>
    </>
  );
}
