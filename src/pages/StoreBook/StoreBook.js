import React, { useState, useEffect } from "react";
import { Table, Form, Button, Tabs, Tab, Row, Col } from "react-bootstrap";
import Moment from "moment";
import axios from "axios";
import BaseUrl from "../../BaseUrl";
import { Link } from "react-router-dom";
import { Card } from "react-bootstrap";
import ReactPaginate from "react-paginate";

export default function StoreBook() {
  const [key, setKey] = useState("list-view");
  const [onEdit, setOnEdit] = useState(false);

  const [storebook, setStoreBook] = useState([]);
  const [storeBookName,setStoreBookName]= useState("");
  const [storeBookId, setStoreBookId] = useState("");
  const [bookAuthor,setBookAuthor] = useState("");
  const [bookBanner,setBookBanner]= useState("");
  const [description, setDescription] = useState("");
  const [discount, setDiscount] = useState("");
  const [handlingFee,setHandlingFee]= useState("");
  const [mrp,setMrp]= useState("");
  const [sellingCost,setSellingCost]= useState("");
  const [fileName, setFileName] = useState("");
  const [subscriptionDays, setSubscriptionDays] =  useState("");
  const [trailPeriod, setTrailPeriod] = useState("");

  const [photoName, setPhotoName] = useState("");
  const [selectedFile, setSelectedFile] = useState(null);
  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

  const ImageUrl = `${BaseUrl}/file/downloadFile/?filePath=`;

  let data = {
    bookAuthor,
    bookBanner,
    createdBy: {
      userId: user.userId,
    },
    description,
    discount,
    fileName,
    handlingFee,
    mrp,
    sellingCost,
    storeBookName,
    subscriptionDays,
    trailPeriod,
  };
  
  const postData = (e) => {
    e.preventDefault();
    axios({
      method: "post",
      url: `${BaseUrl}/storebook/v1/createStoreBook`,
      headers,
      data,
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
      setStoreBookName("");
      setBookAuthor("");
      setBookBanner("");
      setDescription("");
      setDiscount("");
      setHandlingFee("");
      setMrp("");
      setSellingCost("");
      setSubscriptionDays("");
      setTrailPeriod("");
    setPhotoName("");
    setSelectedFile(null);
    setKey("list-view");
    FetchData();
  };

  const FetchData = async () => {
    await axios({
      method: "get",
      url: `${BaseUrl}/storebook/v1/getAllStoreCourseByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setStoreBook(data.content);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    FetchData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    FetchData(pageSize);
  };

  useEffect(() => {
    FetchData();
  }, [bookAuthor, bookBanner,description, discount ,fileName ,handlingFee,  mrp, sellingCost, storeBookName, subscriptionDays,trailPeriod, photoName, pageSize, pageNumber]);

  const handleRemove = (storeBookId) => {
    axios({
      method: "delete",
      url: `${BaseUrl}/storebook/v1/deleteStoreCourseById/${storeBookId}`,
      headers,
    })
      .then((res) => {
        if (res.data.responseCode === 200) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
        FetchData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleEdit = (storeBookId) => {
    axios({
      method: "get",
      url: `${BaseUrl}/storebook/v1/getStoreCourseByStoreCourseId/{storeCourseId}?storeCourseId=${storeBookId}`,
      headers,
    })
      .then(function (res) {
        setStoreBookName(res.data.storeBookName);
        setBookAuthor(res.data.bookAuthor);
        setBookBanner(res.data.bookBanner);
        setDescription(res.data.description);
        setDiscount(res.data.discount);
        setHandlingFee(res.data.handlingFee);
        setMrp(res.data.mrp);
        setSellingCost(res.data.sellingCost);
        setSubscriptionDays(res.data.subscriptionDays);
        setTrailPeriod(res.data.trailPeriod);
        setOnEdit(true);
        setKey("add");
        setStoreBookId(storeBookId);
        FetchData();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const editData = (e) => {
    e.preventDefault();
    axios({
      method: "put",
      url: `${BaseUrl}/storebook/v1/updateStoreBook`,
      headers,
      data,
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setStoreBookName("");
    setBookAuthor("");
    setBookBanner("");
    setDescription("");
    setDiscount("");
    setHandlingFee("");
    setMrp("");
    setSellingCost("");
    setSubscriptionDays("");
    setTrailPeriod("");
    setPhotoName("");
    setSelectedFile(null);
    setKey("list-view");
    FetchData();
  };

  const truncate = (str) => {
    return str.length > 20 ? str.substring(0, 15) + "..." : str;
  };

  const onFileChange = (e) => {
    setSelectedFile(e.target.files[0]);
  };

  const onFileUpload = (e) => {
    e.preventDefault();
    const data = new FormData();

    data.append("file", selectedFile);

    axios({
      mode: "no-cors",
      method: "post",
      url: `${BaseUrl}/file/uploadFile`,
      headers: {
        "content-type": "multipart/form-data",
        Authorization: "Bearer " + user.accessToken,
      },
      data,
    })
      .then(function (res) {
        setPhotoName(res.data.fileName);
        alert(res.data.message);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <>
      <main id="main" className="main">
        <div className={key === "add" ? "pagetitle mb-5" : "pagetitle"}>
          <h1>StoreBook</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/Admin">Home</Link>
              </li>
              <li className="breadcrumb-item active">StoreBook</li>
            </ol>
          </nav>
        </div>
        <div className={key === "list-view" ? "displayForm mt-3" : "hideForm"}>
          <select
            value={value}
            onChange={handleChange}
            style={{ borderColor: "powderblue", borderRadius: 3, padding: 2 }}
          >
            <option value="10">10 / Pages</option>
            <option value="25">25 / Pages</option>
            <option value="50">50 / Pages</option>
            <option value="100">100 / Pages</option>
          </select>
        </div>
        <Card style={{ marginTop: 10 }}>
          <Card.Body>
            <Tabs
              activeKey={key}
              onSelect={(key) => setKey(key)}
              className="mt-3"
            >
              <Tab eventKey="list-view" title="List View">
                <Table striped bordered hover responsive>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th style={{ minWidth: 125 }}>StoreBook Name</th>
                      <th style={{ minWidth: 125 }}>Book Author</th>
                      <th style={{ minWidth: 125 }}>Book Banner</th>
                      <th style={{ minWidth: 125 }}>Subscription Days</th>
                      <th style={{ minWidth: 125 }}>TrailPeriod</th>
                      <th style={{ minWidth: 125 }}>MRP</th>
                      <th style={{ minWidth: 125 }}>Selling Cost</th>
                      <th style={{ minWidth: 125 }}>Discount</th>
                      <th style={{ minWidth: 125 }}>Handling Fee</th>
                      <th style={{ minWidth: 125 }}>File Name</th>
                      <th style={{ minWidth: 150 }}>Description</th>
                      <th>Photo</th>
                      <th style={{ minWidth: 125 }}>Created By</th>
                      <th style={{ minWidth: 125 }}>Updated By</th>
                      <th style={{ minWidth: 125 }}>Inserted Date</th>
                      <th style={{ minWidth: 125 }}>Updated Date</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {storebook.map((data, index) => {
                      return (
                        <tr key={data.storeBookId}>
                          <th scope="row">{index + 1}</th>
                          <td>{data.storeBookName}</td>
                          <td>{data.bookAuthor}</td>
                          <td>{data.bookBanner}</td>
                          <td>{data.subscriptionDays}</td>
                          <td>{data.trailPeriod}</td>
                          <td>{data.mrp}</td>
                          <td>{data.sellingCost}</td>
                          <td>{data.discount}</td>
                          <td>{data.handlingFee}</td>
                          <td>{data.fileName}</td>
                          <td>{truncate(data.description)}</td>
                          <td>
                            <img
                              src={ImageUrl + data.bookBannerPath}
                              alt={data.bookBanner}
                              style={{ width: 100, height: 50 }}
                            />
                          </td>
                          <td>{data.createdBy.userName}</td>
                          <td>{data.updatedBy.userName}</td>
                          <td>
                            {Moment(data.insertedDate).format("DD-MM-YYYY")}
                          </td>
                          <td>
                            {Moment(data.updatedDate).format("DD-MM-YYYY")}
                          </td>
                          <td className="inline-block">
                            <button
                              type="button"
                              className="btn btn-icon btn-sm"
                              title="Edit"
                              onClick={(e) => handleEdit(data.storeBookId, e)}
                            >
                              <i class="bi bi-pencil-square"></i>
                            </button>
                            <button
                              type="button"
                              className="btn btn-icon btn-sm js-sweetalert"
                              title="Delete"
                              onClick={(e) => handleRemove(data.storeBookId, e)}
                            >
                              <i class="bi bi-trash"></i>
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
                <div>
                  <ReactPaginate
                    previousLabel="<"
                    nextLabel=">"
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination justify-content-center"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              </Tab>
              <Tab eventKey="add" title={onEdit ? "Edit" : "Add"}>
                <Form>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>News Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add StoreBook Name"
                      value={storeBookName}
                      onChange={(e) => setStoreBookName(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>News Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Book Author"
                      value={bookAuthor}
                      onChange={(e) => setStoreBookName(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>News Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Book Banner"
                      value={bookBanner}
                      onChange={(e) => setBookBanner(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Subscription Days</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Subscription Days"
                      value={subscriptionDays}
                      onChange={(e) => setSubscriptionDays(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Trail Period</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Trail Period"
                      value={trailPeriod}
                      onChange={(e) => setTrailPeriod(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>StoreBook MRP</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add StoreBook MRP"
                      value={mrp}
                      onChange={(e) => setMrp(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Selling Cost</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Selling Cost"
                      value={sellingCost}
                      onChange={(e) => setSellingCost(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Discount</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Discount"
                      value={discount}
                      onChange={(e) => setDiscount(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Handling Fee</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Handling Fee"
                      value={handlingFee}
                      onChange={(e) => setHandlingFee(e.target.value)}
                    />
                  </Form.Group>
                  


                  <Form.Group className="mb-3 col-12">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add Description"
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                    />
                  </Form.Group>
                  <Row className="align-items-center">
                    <Col>
                      <Form.Group controlId="formFile" className="mb-3">
                        <Form.Label>Select Photo</Form.Label>
                        <Form.Control
                          type="file"
                          onChange={(e) => onFileChange(e)}
                        />
                      </Form.Group>
                    </Col>
                    <Col className="pt-3">
                      <Button
                        variant="primary"
                        onClick={(e) => onFileUpload(e)}
                        type="submit"
                      >
                        Upload
                      </Button>
                    </Col>
                  </Row>

                  {onEdit ? (
                    <Button
                      variant="primary"
                      onClick={(e) => editData(e)}
                      type="submit"
                    >
                      Edit
                    </Button>
                  ) : (
                    <Button
                      variant="primary"
                      onClick={(e) => postData(e)}
                      type="submit"
                    >
                      Submit
                    </Button>
                  )}
                  <Button
                    variant="outline-primary"
                    type="submit"
                    style={{ marginLeft: 15 }}
                    onClick={(e) => setKey(1)}
                  >
                    Cancel
                  </Button>
                </Form>
              </Tab>
            </Tabs>
          </Card.Body>
        </Card>
      </main>
    </>
  );
}
