import React from "react";

export default function Settings() {
  return (
    <>
      <main id="main" className="main d-flex float-left">
        <div className="pagetitle">
          <h1>Settings</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="">Home</a>
              </li>
              <li className="breadcrumb-item active">Settings</li>
            </ol>
          </nav>
        </div>
      </main>
    </>
  );
}
