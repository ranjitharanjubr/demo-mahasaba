import React, { useState, useEffect } from "react";
import { Table, Form, Button, Tabs, Tab, Row, Col } from "react-bootstrap";
import Moment from "moment";
import axios from "axios";
import { Link } from "react-router-dom";
import { Card } from "react-bootstrap";
import ReactPaginate from "react-paginate";

export default function Directors() {
  const [key, setKey] = useState("list-view");
  const [onEdit, setOnEdit] = useState(false);
  const [director, setDirector] = useState([]);
  const [fullName, setFullName] = useState("");
  const [alternatePhoneNumber, setAlternatePhoneNumber] = useState("");
  const [directorId, setDirectorId] = useState("");
  const [description, setDescription] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [emailId, setEmailId] = useState("");
  const [gender, setGender] = useState("");
  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };
  const BaseUrl = "https://executivetracking.cloudjiffy.net/Mahaasabha";

  let data = {
    createdBy: {
      userId: user.userId,
    },
    description,
    emailId,
    gender,

    mobileNumber,

    fullName,
    alternatePhoneNumber,
  };

  const postData = (e) => {
    e.preventDefault();
    axios({
      method: "post",
      url: `${BaseUrl}/director/v1/createDirector`,
      headers,
      data,
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setFullName("");

    setAlternatePhoneNumber("");

    setDescription("");

    setMobileNumber("");

    setKey("list-view");
    FetchData();
  };

  const FetchData = async () => {
    await axios({
      method: "get",
      url: `${BaseUrl}/director/v1/getAllDirectorByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setDirector(data.content);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    FetchData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    FetchData(pageSize);
  };

  useEffect(() => {
    FetchData();
  }, [
    description,
    emailId,
    gender,

    mobileNumber,
    fullName,
    alternatePhoneNumber,

    pageSize,
    pageNumber,
  ]);

  const handleRemove = (directorId) => {
    axios({
      method: "delete",
      url: `${BaseUrl}/director/v1/deleteDirectorByDirectorId/${directorId}`,
      headers,
    })
      .then((res) => {
        if (res.data.responseCode === 200) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
        FetchData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleEdit = (directorId) => {
    axios({
      method: "get",
      url: `https://executivetracking.cloudjiffy.net/Mahaasabha/director/v1/getDirectorByDirectorId/%7BdirectorId%7D?directorId=${directorId}`,
      headers,
    })
      .then(function (res) {
        setFullName(res.data.fullName);
        setAlternatePhoneNumber(res.data.alternatePhoneNumber);
        setDescription(res.data.description);

        setMobileNumber(res.data.mobileNumber);

        setOnEdit(true);
        setKey("add");
        setDirectorId(directorId);
        FetchData();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const editData = (e) => {
    e.preventDefault();
    axios({
      method: "put",
      url: `${BaseUrl}/director/v1/updateDirector`,
      headers,
      data,
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setFullName("");
    setAlternatePhoneNumber("");

    setDescription("");

    setMobileNumber("");

    setKey("list-view");
    FetchData();
  };

  const truncate = (str) => {
    return str.length > 20 ? str.substring(0, 15) + "..." : str;
  };

  return (
    <>
      <main id="main" className="main">
        <div className={key === "add" ? "pagetitle mb-5" : "pagetitle"}>
          <h1>Director</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/Admin">Home</Link>
              </li>
              <li className="breadcrumb-item active">Director</li>
            </ol>
          </nav>
        </div>
        <div className={key === "list-view" ? "displayForm mt-3" : "hideForm"}>
          <select
            value={value}
            onChange={handleChange}
            style={{ borderColor: "powderblue", borderRadius: 3, padding: 2 }}
          >
            <option value="10">10 / Pages</option>
            <option value="25">25 / Pages</option>
            <option value="50">50 / Pages</option>
            <option value="100">100 / Pages</option>
          </select>
        </div>
        <Card style={{ marginTop: 10 }}>
          <Card.Body>
            <Tabs
              activeKey={key}
              onSelect={(key) => setKey(key)}
              className="mt-3"
            >
              <Tab eventKey="list-view" title="List View">
                <Table striped bordered hover responsive>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th style={{ minWidth: 125 }}>Full Name</th>
                      <th style={{ minWidth: 125 }}> EmailId</th>
                      <th style={{ minWidth: 125 }}>Gender</th>
                      <th style={{ minWidth: 150 }}>Description</th>
                      <th style={{ minWidth: 125 }}>Mobile Number</th>

                      <th style={{ minWidth: 125 }}>Created By</th>
                      <th style={{ minWidth: 125 }}>Updated By</th>
                      <th style={{ minWidth: 125 }}>Inserted Date</th>
                      <th style={{ minWidth: 125 }}>Updated Date</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {director.map((data, index) => {
                      return (
                        <tr key={data.directorId}>
                          <th scope="row">{index + 1}</th>
                          <td>{data.fullName}</td>
                          <td>{data.alternatePhoneNumber}</td>
                          <td>{data.emailId}</td>
                          <td>{data.gender}</td>
                          <td>{truncate(data.description)}</td>

                          <td>{data.mobileNumber}</td>

                          {/* <td>{data.createdBy.userName}</td>
                          <td>{data.updatedBy.userName}</td> */}
                          <td>{}</td>
                          <td>
                            {Moment(data.insertedDate).format("DD-MM-YYYY")}
                          </td>
                          <td>
                            {Moment(data.updatedDate).format("DD-MM-YYYY")}
                          </td>
                          <td className="inline-block">
                            <button
                              type="button"
                              className="btn btn-icon btn-sm"
                              title="Edit"
                              onClick={(e) => handleEdit(data.directorId, e)}
                            >
                              <i class="bi bi-pencil-square"></i>
                            </button>
                            <button
                              type="button"
                              className="btn btn-icon btn-sm js-sweetalert"
                              title="Delete"
                              onClick={(e) => handleRemove(data.directorId, e)}
                            >
                              <i class="bi bi-trash"></i>
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
                <div>
                  <ReactPaginate
                    previousLabel="<"
                    nextLabel=">"
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination justify-content-center"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              </Tab>
              <Tab eventKey="add" title={onEdit ? "Edit" : "Add"}>
                <Form>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Full Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Full Name"
                      value={fullName}
                      onChange={(e) => setFullName(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Alternate PhoneNumber</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Alternate PhoneNumber"
                      value={alternatePhoneNumber}
                      onChange={(e) => setAlternatePhoneNumber(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>EmailId</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add emailId"
                      value={emailId}
                      onChange={(e) => setEmailId(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Gender</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add gender"
                      value={gender}
                      onChange={(e) => setGender(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Description"
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Mobile Number"
                      value={mobileNumber}
                      onChange={(e) => setMobileNumber(e.target.value)}
                    />
                  </Form.Group>

                  <Row className="align-items-center">
                    <Col>
                      {/* <Form.Group controlId="formFile" className="mb-3">
                        <Form.Label>Select Photo</Form.Label>
                        <Form.Control
                          type="file"
                          onChange={(e) => onFileChange(e)}
                        />
                      </Form.Group> */}
                    </Col>
                    {/* <Col className="pt-3">
                      <Button
                        variant="primary"
                        onClick={(e) => onFileUpload(e)}
                        type="submit"
                      >
                        Upload
                      </Button>
                    </Col> */}
                  </Row>

                  {onEdit ? (
                    <Button
                      variant="primary"
                      onClick={(e) => editData(e)}
                      type="submit"
                    >
                      Edit
                    </Button>
                  ) : (
                    <Button
                      variant="primary"
                      onClick={(e) => postData(e)}
                      type="submit"
                    >
                      Submit
                    </Button>
                  )}
                  <Button
                    variant="outline-primary"
                    type="submit"
                    style={{ marginLeft: 15 }}
                    onClick={(e) => setKey(1)}
                  >
                    Cancel
                  </Button>
                </Form>
              </Tab>
            </Tabs>
          </Card.Body>
        </Card>
      </main>
    </>
  );
}






























import React, { useState, useEffect } from "react";
import { Table, Form, Button, Tabs, Tab, Row, Col } from "react-bootstrap";
import Moment from "moment";
import axios from "axios";
//import BaseUrl from "../../BaseUrl";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import ReactPaginate from "react-paginate";

export default function Directors() {
  const [key, setKey] = useState("list-view");
  const [fullName, setFullName] = useState("");
  const [gender,setGender] =useState("");
  const [director, setDirector] = useState([]);
  const [description, setDescription] = useState("");
  const [emailId, setEmailId] = useState("");
  const [mobileNumber,setMobileNumber] = useState("");
  const [alternativeMobileNumber,setAlternativeMobileNumber] = useState("");
  const [onEdit, setOnEdit] = useState(false);
  const [directorId, setDirectorId] = useState("");
  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);
  const [fileName, setFileName] = useState("");
  const [selectedFile, setselectedFile] = useState(null);

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };
  const BaseUrl="https://executivetracking.cloudjiffy.net/Mahaasabha"

  const profilePicNameUrl = `${BaseUrl}/file/downloadFile/?filePath=`;

  const postData = (e) => {
    e.preventDefault();
    axios({
      method: "post",
      url: `${BaseUrl}/director/v1/createDirector`,
      headers,
      data: {
        fullName,
        gender,
        createdBy: {
          userId: user.userId,
        },
        description,
        emailId,
        mobileNumber,
        alternativeMobileNumber,
        fileName,
      },
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setFullName("");
    setGender("");
    setDescription("");
    setEmailId("");
    setMobileNumber("");
    setAlternativeMobileNumber("");
    setselectedFile(null);
    setFileName("");
    setKey("list-view");
    FetchData();
  };

  const FetchData = async () => {
    await axios({
      method: "get",
      url: `${BaseUrl}/director/v1/getAllDirectorByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setDirector(data.content);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    FetchData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    FetchData(pageSize);
  };

  useEffect(() => {
    FetchData();
  }, [fullName,gender, description,emailId,mobileNumber,alternativeMobileNumber, pageSize, pageNumber]);

  const handleRemove = (directorId) => {
    axios({
      method: "delete",
      url: `${BaseUrl}/director/v1/deleteDirectorByDirectorId/{directorId}?directorId=${directorId}`,
      headers,
    })
      .then((res) => {
        if (res.data.responseCode === 200) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
        FetchData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleEdit = (directorId) => {
    axios({
      method: "get",
      url: `${BaseUrl}/director/v1/getDirectorByDirectorId/{directorId}?directorId=${directorId}`,
      headers,
    })
      .then(function (res) {
        setFullName(res.data.fullName);
        setGender(res.data.gender);
        setDescription(res.data.description);
        setEmailId(res.data.emailId);
        setMobileNumber(res.data.mobileNumber)
        setAlternativeMobileNumber(res.data.alternativeMobileNumber)
        setFileName(res.data.fileName);
        setOnEdit(true);
        setKey("add");
        setDirectorId(directorId);
        FetchData();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const onFileChange = (e) => {
    setselectedFile(e.target.files[0]);
  };

  const onFileUpload = (e) => {
    e.preventDefault();
    const data = new FormData();

    data.append("file", selectedFile);

    axios({
      mode: "no-cors",
      method: "post",
      url: `${BaseUrl}/file/uploadFile`,
      headers: {
        "content-type": "multipart/form-data",
        Authorization: "Bearer " + user.accessToken,
      },
      data,
    })
      .then(function (res) {
        setFileName(res.data.fileName);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const editData = (e) => {
    e.preventDefault();
    axios({
      method: "put",
      url: `${BaseUrl}/director/v1/updateDirector`,
      headers,
      data: {
        description,
        emailId,
        mobileNumber,
        alternativeMobileNumber,
        directorId,
        fullName,
        gender,
        updatedBy: {
          userId: user.userId,
        },
      },
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setFullName("");
    setGender("");
    setDescription("");
    setEmailId("");
    setMobileNumber("");
    setAlternativeMobileNumber("");
    setselectedFile(null);
    setFileName("");
    setOnEdit(false);
    setKey("list-view");
    FetchData();
  };

  return (
    <>
      <main id="main" className="main">
        <div className={key === "add" ? "pagetitle mb-5" : "pagetitle"}>
          <h1>Director</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/Admin/adsmenu">AdsMenu</Link>
              </li>
              <li className="breadcrumb-item active">Ads</li>
            </ol>
          </nav>
        </div>
        <div className={key === "list-view" ? "displayForm mt-3" : "hideForm"}>
          <select
            value={value}
            onChange={handleChange}
            style={{ borderColor: "powderblue", borderRadius: 3, padding: 2 }}
          >
            <option value="10">10 / Pages</option>
            <option value="25">25 / Pages</option>
            <option value="50">50 / Pages</option>
            <option value="100">100 / Pages</option>
          </select>
        </div>
        <Card style={{ marginTop: 10 }}>
          <Card.Body>
            <Tabs
              activeKey={key}
              onSelect={(key) => setKey(key)}
              className="mt-3"
            >
              <Tab eventKey="list-view" title="List View">
                <Table striped bordered hover scroll responsive>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Full Name</th>
                      <th>Description</th>
                      <th>EmailId</th>
                      <th>MobileNumber</th>
                      <th>AlternativeMobileNumber</th>
                      <th>ProfilePicName</th>
                      
                      <th>Created By</th>
                      <th>Updated By</th>
                      <th>Inserted Date</th>
                      <th>Updated Date</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {director.map((data, index) => {
                      return (
                        <tr key={data.directorId}>
                          <th scope="row">{index + 1}</th>
                          <td>{data.fullName}</td>
                          <td>{data.gender}</td>
                          <td
                            style={{
                              "max-width": "200px",
                              overflow: "hidden",
                              "text-overflow": "ellipsis",
                              height: "56px",
                            }}
                          >
                            {data.description}
                          </td>
                          <td>
                            <td>{data.emailId}</td>
                            <td>{data.mobileNumber}</td>
                            <td>{data.alternativeMobileNumber}</td>
                            <img
                              src={profilePicNameUrl + data.filePath}
                              alt={data.fileName}
                              style={{ width: 100, height: 50 }}
                            />
                          </td>
                          <td>{}</td>
                          <td>{}</td>
                          <td>{data.createdBy.userName}</td>
                          <td>{data.updatedBy.userName}</td>
                          <td>
                            {Moment(data.insertedDate).format("DD-MM-YYYY")}
                          </td>
                          <td>
                            {Moment(data.updatedDate).format("DD-MM-YYYY")}
                          </td>
                          <td>
                            <button
                              type="button"
                              className="btn btn-icon btn-sm"
                              title="Edit"
                              onClick={(e) =>
                                handleEdit(data.directorId, e)
                              }
                            >
                              <i class="bi bi-pencil-square"></i>
                            </button>
                            <button
                              type="button"
                              className="btn btn-icon btn-sm js-sweetalert"
                              title="Delete"
                              onClick={(e) =>
                                handleRemove(data.directorId, e)
                              }
                            >
                              <i class="bi bi-trash"></i>
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
                <div>
                  <ReactPaginate
                    previousLabel="<"
                    nextLabel=">"
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination justify-content-center"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              </Tab>
              <Tab eventKey="add" title={onEdit ? "Edit" : "Add"}>
                <Form>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Full Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Full Name"
                      value={fullName}
                      onChange={(e) => setFullName(e.target.value)}
                    />
                    {/* <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text> */}
                  </Form.Group>

                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Gender</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Full Name"
                      value={gender}
                      onChange={(e) => setGender(e.target.value)}
                    />
                    {/* <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text> */}
                  </Form.Group>

                  <Form.Group className="mb-3 col-12">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add Description"
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                    />
                  </Form.Group>


                  <Form.Group className="mb-3 col-12">
                    <Form.Label>EmailId</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add EmailId"
                      value={emailId}
                      onChange={(e) => setEmailId(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3 col-12">
                    <Form.Label>MobileNumber</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add MobileNumber"
                      value={mobileNumber}
                      onChange={(e) => setMobileNumber(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3 col-12">
                    <Form.Label>AlternativeMobileNumber</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add AlternativeMobileNumber"
                      value={alternativeMobileNumber}
                      onChange={(e) => setAlternativeMobileNumber(e.target.value)}
                    />
                  </Form.Group>

                  <Row className="align-items-center">
                    <Col>
                      <Form.Group controlId="formFile" className="mb-3">
                        <Form.Label>Select File</Form.Label>
                        <Form.Control
                          type="file"
                          onChange={(e) => onFileChange(e)}
                        />
                      </Form.Group>
                    </Col>
                    <Col className="pt-3">
                      <Button
                        variant="primary"
                        onClick={(e) => onFileUpload(e)}
                        type="submit"
                      >
                        Upload
                      </Button>
                    </Col>
                  </Row>
                  {onEdit ? (
                    <Button
                      variant="primary"
                      onClick={(e) => editData(e)}
                      type="submit"
                    >
                      Edit
                    </Button>
                  ) : (
                    <Button
                      variant="primary"
                      onClick={(e) => postData(e)}
                      type="submit"
                    >
                      Submit
                    </Button>
                  )}
                  <Button
                    variant="outline-primary"
                    type="submit"
                    style={{ marginLeft: 15 }}
                    onClick={(e) => setKey(1)}
                  >
                    Cancel
                  </Button>
                </Form>
              </Tab>
            </Tabs>
          </Card.Body>
        </Card>
      </main>
    </>
  );
}
