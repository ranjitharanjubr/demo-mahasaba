import React, { useState, useEffect } from "react";
import { Table, Form, Button, Tabs, Tab, Row, Col } from "react-bootstrap";
import Moment from "moment";
import axios from "axios";
import BaseUrl from "../../BaseUrl";
import { Link } from "react-router-dom";
import { Card } from "react-bootstrap";
import ReactPaginate from "react-paginate";
import Select from "react-select";

export default function Module() {
  const [key, setKey] = useState("list-view");
  const [onEdit, setOnEdit] = useState(false);

  const [module, setModule] = useState([]);
  const [moduleName, setModuleName] = useState("");
  const [description, setDescription] = useState("");
  const [moduleId, setModuleId] = useState("");

  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);

  const [filteredData, setFilteredData] = useState("");

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

  let data = {
    moduleName,
    createdBy: {
      userId: user.userId,
    },
    description,
  };

  const postData = (e) => {
    e.preventDefault();
    axios({
      method: "post",
      url: `${BaseUrl}/module/v1/createModule`,
      headers,
      data,
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setModuleName("");
    setDescription("");
    setKey("list-view");
    FetchData();
  };

  const FetchData = async () => {
    await axios({
      method: "get",
      url: `${BaseUrl}/module/v1/getAllModuleByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setModule(data.content);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    FetchData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    FetchData(pageSize);
  };

  useEffect(() => {
    FetchData();
  }, [moduleName, description, pageSize, pageNumber]);

  const handleRemove = (moduleId) => {
    axios({
      method: "delete",
      url: `${BaseUrl}/module/v1/deleteModuleById/${moduleId}`,
      headers,
    })
      .then((res) => {
        if (res.data.responseCode === 200) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
        FetchData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleEdit = (moduleId) => {
    axios({
      method: "get",
      url: `${BaseUrl}/module/v1/getModuleByModuleId/{moduleId}?moduleId=${moduleId}`,
      headers,
    })
      .then(function (res) {
        setModuleName(res.data.moduleName);
        setDescription(res.data.description);

        setOnEdit(true);
        setKey("add");
        setModuleId(moduleId);
        FetchData();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const editData = (e) => {
    e.preventDefault();
    axios({
      method: "put",
      url: `${BaseUrl}/module/v1/updateModule`,
      headers,
      data,
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setModuleName("");
    setDescription("");

    setKey("list-view");
    FetchData();
  };

  const truncate = (str) => {
    return str.length > 20 ? str.substring(0, 15) + "..." : str;
  };

  const [searchParams] = useState(["moduleName"]);

  return (
    <>
      <main id="main" className="main">
        <div className={key === "add" ? "pagetitle mb-5" : "pagetitle"}>
          <h1>Module</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/Admin/course-menu">Course Menu</Link>
              </li>
              <li className="breadcrumb-item active">Module</li>
            </ol>
          </nav>
        </div>
        <Form>
          <Row>
            <Col>
              <div
                className={
                  key === "list-view" ? "displayForm mt-1" : "hideForm"
                }
              >
                <select
                  value={value}
                  onChange={handleChange}
                  style={{
                    borderColor: "powderblue",
                    borderRadius: 3,
                    padding: 7,
                  }}
                >
                  <option value="10">10 / Pages</option>
                  <option value="25">25 / Pages</option>
                  <option value="50">50 / Pages</option>
                  <option value="100">100 / Pages</option>
                </select>
              </div>
            </Col>
            <Col xs={3}>
              <Form.Group className={key === "list-view" ? "mb-3" : "hideForm"}>
                <Form.Control
                  size="md"
                  type="text"
                  placeholder="Search..."
                  value={filteredData}
                  onChange={(e) => setFilteredData(e.target.value)}
                />
              </Form.Group>
            </Col>
          </Row>
        </Form>
        <Card style={{ marginTop: 10 }}>
          <Card.Body>
            <Tabs
              activeKey={key}
              onSelect={(key) => setKey(key)}
              className="mt-3"
            >
              <Tab eventKey="list-view" title="List View">
                <Table striped bordered hover responsive>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th style={{ minWidth: 125 }}>Module Name</th>
                      <th style={{ minWidth: 150 }}>Description</th>
                      <th style={{ minWidth: 125 }}>Created By</th>
                      <th style={{ minWidth: 125 }}>Updated By</th>
                      <th style={{ minWidth: 125 }}>Inserted Date</th>
                      <th style={{ minWidth: 125 }}>Updated Date</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {module
                      .filter((item) => {
                        return searchParams.some((newItem) => {
                          return (
                            item[newItem]
                              .toString()
                              .toLowerCase()
                              .indexOf(filteredData.toLowerCase()) > -1
                          );
                        });
                      })
                      .map((data, index) => {
                        return (
                          <tr key={data.moduleId}>
                            <th scope="row">{index + 1}</th>
                            <td>{data.moduleName}</td>
                            <td>{truncate(data.description)}</td>

                            <td>{data.createdBy.userName}</td>
                            <td>{data.updatedBy.userName}</td>
                            <td>
                              {Moment(data.insertedDate).format("DD-MM-YYYY")}
                            </td>
                            <td>
                              {Moment(data.updatedDate).format("DD-MM-YYYY")}
                            </td>
                            <td className="inline-block">
                              <button
                                type="button"
                                className="btn btn-icon btn-sm"
                                title="Edit"
                                onClick={(e) => handleEdit(data.moduleId, e)}
                              >
                                <i class="bi bi-pencil-square"></i>
                              </button>
                              <button
                                type="button"
                                className="btn btn-icon btn-sm js-sweetalert"
                                title="Delete"
                                onClick={(e) => handleRemove(data.moduleId, e)}
                              >
                                <i class="bi bi-trash"></i>
                              </button>
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
                <div>
                  <ReactPaginate
                    previousLabel="<"
                    nextLabel=">"
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination justify-content-center"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              </Tab>
              <Tab eventKey="add" title={onEdit ? "Edit" : "Add"}>
                <Form>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Module Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Module Name"
                      value={moduleName}
                      onChange={(e) => setModuleName(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-12">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add Description"
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                    />
                  </Form.Group>

                  {onEdit ? (
                    <Button
                      variant="primary"
                      onClick={(e) => editData(e)}
                      type="submit"
                    >
                      Edit
                    </Button>
                  ) : (
                    <Button
                      variant="primary"
                      onClick={(e) => postData(e)}
                      type="submit"
                    >
                      Submit
                    </Button>
                  )}
                  <Button
                    variant="outline-primary"
                    type="submit"
                    style={{ marginLeft: 15 }}
                    onClick={(e) => setKey(1)}
                  >
                    Cancel
                  </Button>
                </Form>
              </Tab>
            </Tabs>
          </Card.Body>
        </Card>
      </main>
    </>
  );
}
