import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import BaseUrl from "../../BaseUrl";

export default function CourseMenu() {
  const [countData, setCountData] = useState([]);

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

  const fetchCountData = async () => {
    await axios({
      method: "get",
      url: `${BaseUrl}/dashboard/v1/courseModuleDashboard`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setCountData(data);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchCountData();
  }, []);

  let navigate = useNavigate();
  return (
    <>
      <main id="main" className="main" style={{ display: "block" }}>
        <div className="pagetitle mb-5">
          <h1>Course Menu</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/Admin">Home</Link>
              </li>
              <li className="breadcrumb-item active">Course Menu</li>
            </ol>
          </nav>
        </div>

        <section class="section dashboard">
          <div class="col-lg-12">
            <div class="row">
              <div
                class="col-xxl-4 col-md-2"
                onClick={(e) => navigate("/Admin/course")}
              >
                <div class="card info-card sales-card">
                  <div class="card-body">
                    <h5 class="card-title">Course</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-list-columns"></i>
                      </div>
                      <div class="ps-3">
                        <h6>{countData.courseCount}</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div
                class="col-xxl-4 col-md-2"
                onClick={(e) => navigate("/Admin/module")}
              >
                <div class="card info-card revenue-card">
                  <div class="card-body">
                    <h5 class="card-title">Module</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-percent"></i>
                      </div>
                      <div class="ps-3">
                        <h6>{countData.moduleCount}</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div
                class="col-xxl-4 col-md-2"
                onClick={(e) => navigate("/Admin/topic")}
              >
                <div class="card info-card customers-card">
                  <div class="card-body">
                    <h5 class="card-title">Topic</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-translate"></i>
                      </div>
                      <div class="ps-3">
                        <h6>{countData.topicCount}</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </>
  );
}
