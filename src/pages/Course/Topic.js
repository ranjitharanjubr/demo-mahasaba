import React, { useState, useEffect } from "react";
import { Table, Form, Button, Tabs, Tab, Row, Col } from "react-bootstrap";
import Moment from "moment";
import axios from "axios";
import BaseUrl from "../../BaseUrl";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import ReactPaginate from "react-paginate";

export default function Topic() {
  const [key, setKey] = useState("list-view");
  const [onEdit, setOnEdit] = useState(false);

  const [topic, setTopic] = useState([]);
  const [topicName, setTopicName] = useState("");
  const [description, setDescription] = useState("");
  const [url, setUrl] = useState("");
  const [videoUrl, setVideoUrl] = useState("");
  const [topicId, setTopicId] = useState("");
  const [fileName, setFileName] = useState("");
  const [selectedFile, setselectedFile] = useState(null);

  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

  let data = {
    createdBy: {
      userId: user.userId,
    },
    description,
    fileName,
    topicName,
    url,
    videoUrl,
  };

  const postData = (e) => {
    e.preventDefault();
    axios({
      method: "post",
      url: `${BaseUrl}/topic/v1/createTopic`,
      headers,
      data,
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setTopicName("");
    setDescription("");
    setselectedFile(null);
    setFileName("");
    setUrl("");
    setVideoUrl("");
    setKey("list-view");
    FetchData();
  };

  const FetchData = async () => {
    await axios({
      method: "get",
      url: `${BaseUrl}/topic/v1/getAllTopicByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setTopic(data.content);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    FetchData(pageNumber);
  };

  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    FetchData(pageSize);
  };

  useEffect(() => {
    FetchData();
  }, [topicName, description, url, videoUrl, fileName, pageSize, pageNumber]);

  const handleRemove = (topicId) => {
    axios({
      method: "delete",
      url: `${BaseUrl}/topic/v1/deleteTopicById/${topicId}`,
      headers,
    })
      .then((res) => {
        if (res.data.responseCode === 200) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
        FetchData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleEdit = (topicId) => {
    axios({
      method: "get",
      url: `${BaseUrl}/topic/v1/getTopicByTopicId/{topicId}?topicId=${topicId}`,
      headers,
    })
      .then(function (res) {
        setTopicName(res.data.topicName);
        setDescription(res.data.description);
        setFileName(res.data.fileName);
        setUrl(res.data.url);
        setVideoUrl(res.data.videoUrl);
        setOnEdit(true);
        setKey("add");
        setTopicId(topicId);
        FetchData();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const editData = (e) => {
    e.preventDefault();
    axios({
      method: "put",
      url: `${BaseUrl}/topic/v1/updateTopic`,
      headers,
      data,
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setTopicName("");
    setDescription("");
    setselectedFile(null);
    setFileName("");
    setUrl("");
    setVideoUrl("");
    setKey("list-view");
    FetchData();
  };

  const truncate = (str) => {
    return str.length > 20 ? str.substring(0, 15) + "..." : str;
  };

  const onFileChange = (e) => {
    console.log(e.target.files[0]);
    setselectedFile(e.target.files[0]);
  };

  const onFileUpload = (e) => {
    e.preventDefault();
    const data = new FormData();

    data.append("file", selectedFile);

    axios({
      mode: "no-cors",
      method: "post",
      url: `${BaseUrl}/file/uploadFile`,
      headers: {
        "content-type": "multipart/form-data",
        Authorization: "Bearer " + user.accessToken,
      },
      data,
    })
      .then(function (res) {
        setFileName(res.data.fileName);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <>
      <main id="main" className="main">
        <div className={key === "add" ? "pagetitle mb-5" : "pagetitle"}>
          <h1>Topic</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/Admin/course-menu">Course Menu</Link>
              </li>
              <li className="breadcrumb-item active">Topic</li>
            </ol>
          </nav>
        </div>
        <div className={key === "list-view" ? "displayForm mt-3" : "hideForm"}>
          <select
            value={value}
            onChange={handleChange}
            style={{ borderColor: "powderblue", borderRadius: 3, padding: 2 }}
          >
            <option value="10">10 / Pages</option>
            <option value="25">25 / Pages</option>
            <option value="50">50 / Pages</option>
            <option value="100">100 / Pages</option>
          </select>
        </div>
        <Card style={{ marginTop: 10 }}>
          <Card.Body>
            <Tabs
              activeKey={key}
              onSelect={(key) => setKey(key)}
              className="mt-3"
            >
              <Tab eventKey="list-view" title="List View">
                <Table striped bordered hover responsive>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th style={{ minWidth: 125 }}>Topic Name</th>
                      <th style={{ minWidth: 150 }}>Description</th>
                      <th style={{ minWidth: 150 }}>File Name</th>
                      <th style={{ minWidth: 150 }}>url</th>
                      <th style={{ minWidth: 150 }}>Video Url</th>
                      <th style={{ minWidth: 125 }}>Created By</th>
                      <th style={{ minWidth: 125 }}>Updated By</th>
                      <th style={{ minWidth: 125 }}>Inserted Date</th>
                      <th style={{ minWidth: 125 }}>Updated Date</th>
                      <th style={{ minWidth: 90 }}>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {topic.map((data, index) => {
                      return (
                        <tr key={data.topicId}>
                          <th scope="row">{index + 1}</th>
                          <td>{data.topicName}</td>
                          <td>{truncate(data.description)}</td>
                          <td>{truncate(data.fileName)}</td>
                          <td>{truncate(data.url)}</td>
                          <td>
                            <iframe
                              width="100"
                              height="70"
                              src={`https://www.youtube.com/embed/${data.videoUrl}`}
                              frameBorder="0"
                              allowFullScreen
                              title="Embedded youtube"
                            />
                          </td>
                          <td>{data.createdBy.userName}</td>
                          <td>{data.updatedBy.userName}</td>
                          <td>
                            {Moment(data.insertedDate).format("DD-MM-YYYY")}
                          </td>
                          <td>
                            {Moment(data.updatedDate).format("DD-MM-YYYY")}
                          </td>
                          <td className="inline-block">
                            <button
                              type="button"
                              className="btn btn-icon btn-sm"
                              title="Edit"
                              onClick={(e) => handleEdit(data.topicId, e)}
                            >
                              <i class="bi bi-pencil-square"></i>
                            </button>
                            <button
                              type="button"
                              className="btn btn-icon btn-sm js-sweetalert"
                              title="Delete"
                              onClick={(e) => handleRemove(data.topicId, e)}
                            >
                              <i class="bi bi-trash"></i>
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
                <div>
                  <ReactPaginate
                    previousLabel="<"
                    nextLabel=">"
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination justify-content-center"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              </Tab>
              <Tab eventKey="add" title={onEdit ? "Edit" : "Add"}>
                <Form>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Topic Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Topic Name"
                      value={topicName}
                      onChange={(e) => setTopicName(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-12">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add Description"
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Url</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Url"
                      value={url}
                      onChange={(e) => setUrl(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Video Url</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Video Url"
                      value={videoUrl}
                      onChange={(e) => setVideoUrl(e.target.value)}
                    />
                  </Form.Group>
                  <Row className="align-items-center">
                    <Col>
                      <Form.Group controlId="formFile" className="mb-3">
                        <Form.Label>Select File</Form.Label>
                        <Form.Control
                          type="file"
                          onChange={(e) => onFileChange(e)}
                        />
                      </Form.Group>
                    </Col>
                    <Col className="pt-3">
                      <Button
                        variant="primary"
                        onClick={(e) => onFileUpload(e)}
                        type="submit"
                      >
                        Upload
                      </Button>
                    </Col>
                  </Row>
                  {onEdit ? (
                    <Button
                      variant="primary"
                      onClick={(e) => editData(e)}
                      type="submit"
                    >
                      Edit
                    </Button>
                  ) : (
                    <Button
                      variant="primary"
                      onClick={(e) => postData(e)}
                      type="submit"
                    >
                      Submit
                    </Button>
                  )}
                  <Button
                    variant="outline-primary"
                    type="submit"
                    style={{ marginLeft: 15 }}
                    onClick={(e) => setKey(1)}
                  >
                    Cancel
                  </Button>
                </Form>
              </Tab>
            </Tabs>
          </Card.Body>
        </Card>
      </main>
    </>
  );
}
