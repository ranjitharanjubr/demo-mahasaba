import React, { useState, useEffect } from "react";
import { Table, Form, Button, Tabs, Tab, Row, Col } from "react-bootstrap";
import Moment from "moment";
import axios from "axios";
import BaseUrl from "../../BaseUrl";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import ReactPaginate from "react-paginate";
import Select from "react-select";

export default function Course() {
  const [key, setKey] = useState("list-view");
  const [onEdit, setOnEdit] = useState(false);

  const [course, setCourse] = useState([]);
  const [courseName, setCourseName] = useState("");
  const [description, setDescription] = useState("");
  const [courseMrp, setCourseMrp] = useState("");
  const [discount, setDiscount] = useState("");
  const [sellingPrice, setSellingPrice] = useState("");
  const [handlingFee, setHandlingFee] = useState("");
  const [position, setPosition] = useState("");
  const [videoUrl, setVideoUrl] = useState("");
  const [subscriptionDays, setSubscriptionDays] = useState("");
  const [trailPeriod, setTrailPeriod] = useState("");
  const [courseId, setCourseId] = useState("");

  const [categoryId, setCategoryId] = useState("");
  const [category, setCategory] = useState([]);
  const [catValue, setCatValue] = useState(0);

  const [languageId, setLanguageId] = useState("");
  const [language, setLanguage] = useState([]);
  const [langValue, setLangValue] = useState(0);

  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);

  const [filteredData, setFilteredData] = useState("");

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

  let data = {
    courseMrp,
    courseName,
    createdBy: {
      userId: user.userId,
    },
    description,
    discount,
    sellingPrice,
    handlingFee,
    position,
    subscriptionDays,
    trailPeriod,
    videoUrl,
    courseCategoryDto: {
      categoryId,
    },
    courseLanguageDto: {
      languageId,
    },
  };

  const postData = (e) => {
    e.preventDefault();
    axios({
      method: "post",
      url: `${BaseUrl}/course/v1/createCourse`,
      headers,
      data,
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setCourseName("");
    setDescription("");
    setCourseMrp("");
    setDiscount("");
    setSellingPrice("");
    setHandlingFee("");
    setVideoUrl("");
    setPosition("");
    setSubscriptionDays("");
    setTrailPeriod("");
    setCatValue(0);
    setLangValue(0);
    setCategoryId("");
    setLanguageId("");
    setKey("list-view");
    FetchData();
  };

  const FetchData = async () => {
    await axios({
      method: "get",
      url: `${BaseUrl}/course/v1/getAllCourseByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setCourse(data.content);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const FetchCategory = () => {
    axios({
      method: "get",
      url: `${BaseUrl}/category/v1/queryAllCategory`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setCategory(data);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const FetchLanguage = () => {
    axios({
      method: "get",
      url: `${BaseUrl}/language/v1/getAllLanguage`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setLanguage(data);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    FetchData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    FetchData(pageSize);
  };
  const handleCategoryChange = (e) => {
    setCategoryId(e.target.value);
    setCatValue(e.target.value);
  };
  const handleLanguageChange = (e) => {
    setLanguageId(e.target.value);
    setLangValue(e.target.value);
  };

  useEffect(() => {
    FetchData();
    FetchCategory();
    FetchLanguage();
  }, [
    courseName,
    description,
    courseMrp,
    discount,
    sellingPrice,
    handlingFee,
    position,
    videoUrl,
    categoryId,
    languageId,
    subscriptionDays,
    trailPeriod,
    pageSize,
    pageNumber,
  ]);

  const handleRemove = (courseId) => {
    axios({
      method: "delete",
      url: `${BaseUrl}/course/v1/deleteCourseById/${courseId}`,
      headers,
    })
      .then((res) => {
        if (res.data.responseCode === 200) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
        FetchData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleEdit = (courseId) => {
    axios({
      method: "get",
      url: `${BaseUrl}/course/v1/getCourseByCourseId/{courseId}?courseId=${courseId}`,
      headers,
    })
      .then(function (res) {
        setCourseName(res.data.courseName);
        setDescription(res.data.description);
        setCourseMrp(res.data.courseMrp);
        setDiscount(res.data.discount);
        setSellingPrice(res.data.sellingPrice);
        setHandlingFee(res.data.handlingFee);
        setPosition(res.data.position);
        setVideoUrl(res.data.videoUrl);
        setSubscriptionDays(res.data.subscriptionDays);
        setTrailPeriod(res.data.trailPeriod);
        setCategoryId(res.data.courseCategoryDto.categoryId);
        setLanguageId(res.data.courseLanguageDto.languageId);
        setOnEdit(true);
        setKey("add");
        setCourseId(courseId);
        FetchData();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const editData = (e) => {
    e.preventDefault();
    axios({
      method: "put",
      url: `${BaseUrl}/course/v1/updateCourse`,
      headers,
      data,
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setCourseName("");
    setDescription("");
    setCourseMrp("");
    setDiscount("");
    setSellingPrice("");
    setHandlingFee("");
    setPosition("");
    setVideoUrl("");
    setSubscriptionDays("");
    setTrailPeriod("");
    setCategoryId("");
    setLanguageId("");
    setKey("list-view");
    FetchData();
  };

  const truncate = (str) => {
    return str.length > 20 ? str.substring(0, 15) + "..." : str;
  };

  const [searchParams] = useState(["courseName"]);

  return (
    <>
      <main id="main" className="main">
        <div className={key === "add" ? "pagetitle mb-5" : "pagetitle"}>
          <h1>Course</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/Admin/course-menu">Course Menu</Link>
              </li>
              <li className="breadcrumb-item active">Course</li>
            </ol>
          </nav>
        </div>
        <Form>
          <Row>
            <Col>
              <div
                className={
                  key === "list-view" ? "displayForm mt-1" : "hideForm"
                }
              >
                <select
                  value={value}
                  onChange={handleChange}
                  style={{
                    borderColor: "powderblue",
                    borderRadius: 3,
                    padding: 7,
                  }}
                >
                  <option value="10">10 / Pages</option>
                  <option value="25">25 / Pages</option>
                  <option value="50">50 / Pages</option>
                  <option value="100">100 / Pages</option>
                </select>
              </div>
            </Col>
            <Col xs={3}>
              <Form.Group className={key === "list-view" ? "mb-3" : "hideForm"}>
                <Form.Control
                  size="md"
                  type="text"
                  placeholder="Search..."
                  value={filteredData}
                  onChange={(e) => setFilteredData(e.target.value)}
                />
              </Form.Group>
            </Col>
          </Row>
        </Form>
        <Card style={{ marginTop: 10 }}>
          <Card.Body>
            <Tabs
              activeKey={key}
              onSelect={(key) => setKey(key)}
              className="mt-3"
            >
              <Tab eventKey="list-view" title="List View">
                <Table striped bordered hover responsive>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th style={{ minWidth: 125 }}>Course Name</th>
                      <th style={{ minWidth: 150 }}>Description</th>
                      <th>Video Url</th>
                      <th style={{ minWidth: 100, textAlign: "center" }}>
                        MRP
                      </th>
                      <th style={{ minWidth: 100, textAlign: "center" }}>
                        Discount
                      </th>
                      <th style={{ minWidth: 125, textAlign: "center" }}>
                        Selling Price
                      </th>
                      <th style={{ minWidth: 125, textAlign: "center" }}>
                        Handling Fee
                      </th>
                      <th style={{ textAlign: "center" }}>Position</th>
                      <th style={{ minWidth: 160, textAlign: "center" }}>
                        Subscription Days
                      </th>
                      <th style={{ minWidth: 110, textAlign: "center" }}>
                        Trial Period
                      </th>
                      <th>Category</th>
                      <th>Language</th>
                      <th style={{ minWidth: 125 }}>Created By</th>
                      <th style={{ minWidth: 125 }}>Updated By</th>
                      <th style={{ minWidth: 125 }}>Inserted Date</th>
                      <th style={{ minWidth: 125 }}>Updated Date</th>
                      <th style={{ minWidth: 100 }}>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {course
                      .filter((item) => {
                        return searchParams.some((newItem) => {
                          return (
                            item[newItem]
                              .toString()
                              .toLowerCase()
                              .indexOf(filteredData.toLowerCase()) > -1
                          );
                        });
                      })
                      .map((data, index) => {
                        return (
                          <tr key={data.courseId}>
                            <th scope="row">{index + 1}</th>
                            <td>{data.courseName}</td>
                            <td>{truncate(data.description)}</td>
                            <td>
                              <iframe
                                width="100"
                                height="70"
                                src={`https://www.youtube.com/embed/${data.videoUrl}`}
                                frameBorder="0"
                                allowFullScreen
                                title="Embedded youtube"
                              />
                            </td>
                            <td style={{ textAlign: "center" }}>
                              {data.courseMrp}
                            </td>
                            <td style={{ textAlign: "center" }}>
                              {data.discount}
                            </td>
                            <td style={{ textAlign: "center" }}>
                              {data.sellingPrice}
                            </td>
                            <td style={{ textAlign: "center" }}>
                              {data.handlingFee}
                            </td>
                            <td style={{ textAlign: "center" }}>
                              {data.position}
                            </td>
                            <td style={{ textAlign: "center" }}>
                              {data.subscriptionDays}
                            </td>
                            <td style={{ textAlign: "center" }}>
                              {data.trailPeriod}
                            </td>
                            <td>{data.courseCategoryDto.categoryName}</td>
                            <td>{data.courseLanguageDto.languageName}</td>
                            <td>{data.createdBy.userName}</td>
                            <td>{data.updatedBy.userName}</td>
                            <td>
                              {Moment(data.insertedDate).format("DD-MM-YYYY")}
                            </td>
                            <td>
                              {Moment(data.updatedDate).format("DD-MM-YYYY")}
                            </td>
                            <td className="inline-block">
                              <button
                                type="button"
                                className="btn btn-icon btn-sm"
                                title="Edit"
                                onClick={(e) => handleEdit(data.courseId, e)}
                              >
                                <i class="bi bi-pencil-square"></i>
                              </button>
                              <button
                                type="button"
                                className="btn btn-icon btn-sm js-sweetalert"
                                title="Delete"
                                onClick={(e) => handleRemove(data.courseId, e)}
                              >
                                <i class="bi bi-trash"></i>
                              </button>
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
                <div>
                  <ReactPaginate
                    previousLabel="<"
                    nextLabel=">"
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination justify-content-center"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              </Tab>
              <Tab eventKey="add" title={onEdit ? "Edit" : "Add"}>
                <Form>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Course Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Course Name"
                      value={courseName}
                      onChange={(e) => setCourseName(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-12">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add Description"
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                    />
                  </Form.Group>
                  <Row>
                    <Col>
                      <Form.Group className="mb-3">
                        <Form.Label>MRP</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Add Course MRP"
                          value={courseMrp}
                          onChange={(e) => setCourseMrp(e.target.value)}
                        />
                      </Form.Group>
                    </Col>
                    <Col>
                      <Form.Group className="mb-3">
                        <Form.Label>Discount</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Add Discount"
                          value={discount}
                          onChange={(e) => setDiscount(e.target.value)}
                        />
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Form.Group className="mb-3">
                        <Form.Label>Selling Price</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Add Selling Price"
                          value={sellingPrice}
                          onChange={(e) => setSellingPrice(e.target.value)}
                        />
                      </Form.Group>
                    </Col>
                    <Col>
                      <Form.Group className="mb-3 ">
                        <Form.Label>Handling Fee</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Add Handling Fee"
                          value={handlingFee}
                          onChange={(e) => setHandlingFee(e.target.value)}
                        />
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="col-6">
                      <Form.Group className="mb-3">
                        <Form.Label>Position</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Add Position"
                          value={position}
                          onChange={(e) => setPosition(e.target.value)}
                        />
                      </Form.Group>
                    </Col>
                    <Col className="col-6">
                      <Form.Group className="mb-3">
                        <Form.Label>Video Url</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Add Video Url"
                          value={videoUrl}
                          onChange={(e) => setVideoUrl(e.target.value)}
                        />
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Form.Group className="mb-3">
                        <Form.Label>Subscription Days</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Add Subscription Days"
                          value={subscriptionDays}
                          onChange={(e) => setSubscriptionDays(e.target.value)}
                        />
                      </Form.Group>
                    </Col>
                    <Col>
                      <Form.Group className="mb-3">
                        <Form.Label>Trail Period</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Add Trail Period"
                          value={trailPeriod}
                          onChange={(e) => setTrailPeriod(e.target.value)}
                        />
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Form.Group className="mb-3">
                        <Form.Label>Category</Form.Label>
                        <Form.Select
                          catValue={value}
                          onChange={handleCategoryChange}
                        >
                          <option>Select a Category</option>
                          {category.map((data) => {
                            return (
                              <option value={data.categoryId}>
                                {data.categoryName}
                              </option>
                            );
                          })}
                        </Form.Select>
                      </Form.Group>
                    </Col>
                    <Col>
                      <Form.Group className="mb-3">
                        <Form.Label>Language</Form.Label>
                        <Form.Select
                          langValue={value}
                          onChange={handleLanguageChange}
                        >
                          <option>Select a Language</option>
                          {language.map((data) => {
                            return (
                              <option value={data.languageId}>
                                {data.languageName}
                              </option>
                            );
                          })}
                        </Form.Select>
                      </Form.Group>
                    </Col>
                  </Row>

                  {onEdit ? (
                    <Button
                      variant="primary"
                      onClick={(e) => editData(e)}
                      type="submit"
                    >
                      Edit
                    </Button>
                  ) : (
                    <Button
                      variant="primary"
                      onClick={(e) => postData(e)}
                      type="submit"
                    >
                      Submit
                    </Button>
                  )}
                  <Button
                    variant="outline-primary"
                    type="submit"
                    style={{ marginLeft: 15 }}
                    onClick={(e) => setKey(1)}
                  >
                    Cancel
                  </Button>
                </Form>
              </Tab>
            </Tabs>
          </Card.Body>
        </Card>
      </main>
    </>
  );
}
