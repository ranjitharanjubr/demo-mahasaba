import React from "react";
import { Card } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";

export default function AdsMenu() {
  let navigate = useNavigate();
  return (
    <>
      <main id="main" className="main" style={{ display: "block" }}>
        <div className="pagetitle mb-5">
          <h1>Advertisements</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/Admin">Home</Link>
              </li>
              <li className="breadcrumb-item active">Advertisements</li>
            </ol>
          </nav>
        </div>

        <section class="section dashboard">
          <div class="col-lg-12">
            <div class="row">
              <div
                class="col-xxl-4 col-md-2"
                onClick={(e) => navigate("/Admin/advertisement")}
              >
                <div class="card info-card sales-card">
                  <div class="card-body">
                    <h5 class="card-title">Ads</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-badge-ad"></i>
                      </div>
                      <div class="ps-3">
                        <h6>10</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div
                class="col-xxl-4 col-md-2"
                onClick={(e) => navigate("/Admin/promo")}
              >
                <div class="card info-card revenue-card">
                  <div class="card-body">
                    <h5 class="card-title">Promos</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-tv"></i>
                      </div>
                      <div class="ps-3">
                        <h6>5</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </>
  );
}
