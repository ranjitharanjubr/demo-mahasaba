import React, { useState, useEffect } from "react";
import { Table, Form, Button, Tabs, Tab, Row, Col } from "react-bootstrap";
import Moment from "moment";
import axios from "axios";
import BaseUrl from "../../BaseUrl";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import ReactPaginate from "react-paginate";

export default function Promo() {
  const [key, setKey] = useState("list-view");
  const [promoName, setPromoName] = useState("");
  const [promo, setPromo] = useState([]);
  const [description, setDescription] = useState("");
  const [onEdit, setOnEdit] = useState(false);
  const [promoId, setPromoId] = useState("");
  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);
  const [youTube, setYouTube] = useState("");

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

  const postData = (e) => {
    e.preventDefault();
    axios({
      method: "post",
      url: `${BaseUrl}/promo/v1/createPromo`,
      headers,
      data: {
        createdBy: {
          userId: user.userId,
        },
        promoName,
        description,
        youTube,
      },
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setPromoName("");
    setDescription("");
    setYouTube("");
    setKey("list-view");
    FetchData();
  };

  const FetchData = async () => {
    await axios({
      method: "get",
      url: `${BaseUrl}/promo/v1/getAllPromoByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setPromo(data.content);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    FetchData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    FetchData(pageSize);
  };

  useEffect(() => {
    FetchData();
  }, [promoName, description, youTube, pageSize, pageNumber]);

  const handleRemove = (promoId) => {
    axios({
      method: "delete",
      url: `${BaseUrl}/promo/v1/deletePromoById/${promoId}`,
      headers,
    })
      .then((res) => {
        if (res.data.responseCode === 200) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
        FetchData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleEdit = (promoId) => {
    axios({
      method: "get",
      url: `${BaseUrl}/promo/v1/getPromoByPromoId/{promoId}?promoId=${promoId}`,
      headers,
    })
      .then(function (res) {
        setPromoName(res.data.promoName);
        setDescription(res.data.description);
        setYouTube(res.data.youtube);
        setOnEdit(true);
        setKey("add");
        setPromoId(promoId);
        FetchData();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const editData = (e) => {
    e.preventDefault();
    axios({
      method: "put",
      url: `${BaseUrl}/promo/v1/updatePromo`,
      headers,
      data: {
        updatedBy: {
          userId: user.userId,
        },
        description,
        promoId,
        promoName,
        youTube,
      },
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setPromoName("");
    setDescription("");
    setYouTube("");
    setOnEdit(false);
    setKey("list-view");
    FetchData();
  };

  return (
    <>
      <main id="main" className="main">
        <div className={key === "add" ? "pagetitle mb-5" : "pagetitle"}>
          <h1>Advertisements</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/Admin/adsmenu">AdsMenu</Link>
              </li>
              <li className="breadcrumb-item active">Promo</li>
            </ol>
          </nav>
        </div>
        <div className={key === "list-view" ? "displayForm mt-3" : "hideForm"}>
          <select
            value={value}
            onChange={handleChange}
            style={{ borderColor: "powderblue", borderRadius: 3, padding: 2 }}
          >
            <option value="10">10 / Pages</option>
            <option value="25">25 / Pages</option>
            <option value="50">50 / Pages</option>
            <option value="100">100 / Pages</option>
          </select>
        </div>
        <Card style={{ marginTop: 10 }}>
          <Card.Body>
            <Tabs
              activeKey={key}
              onSelect={(key) => setKey(key)}
              className="mt-3"
            >
              <Tab eventKey="list-view" title="List View">
                <Table striped bordered hover responsive>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Promo Name</th>
                      <th>Description</th>
                      <th>Youtube</th>
                      <th>Created By</th>
                      <th>Updated By</th>
                      <th>Inserted Date</th>
                      <th>Updated Date</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {promo.map((data, index) => {
                      return (
                        <tr key={data.promoId}>
                          <th scope="row">{index + 1}</th>
                          <td>{data.promoName}</td>
                          <td
                            style={{
                              "max-width": "200px",
                              overflow: "hidden",
                              "text-overflow": "ellipsis",
                              height: "56px",
                            }}
                          >
                            {data.description}
                          </td>
                          <td>
                            <iframe
                              width="100"
                              height="70"
                              src={`https://www.youtube.com/embed/${data.youTube}`}
                              frameBorder="0"
                              allowFullScreen
                              title="Embedded youtube"
                            />
                          </td>
                          <td>{data.createdBy.userName}</td>
                          <td>{data.updatedBy.userName}</td>
                          <td>
                            {Moment(data.insertedDate).format("DD-MM-YYYY")}
                          </td>
                          <td>
                            {Moment(data.updatedDate).format("DD-MM-YYYY")}
                          </td>
                          <td>
                            <button
                              type="button"
                              className="btn btn-icon btn-sm"
                              title="Edit"
                              onClick={(e) => handleEdit(data.promoId, e)}
                            >
                              <i class="bi bi-pencil-square"></i>
                            </button>
                            <button
                              type="button"
                              className="btn btn-icon btn-sm js-sweetalert"
                              title="Delete"
                              onClick={(e) => handleRemove(data.promoId, e)}
                            >
                              <i class="bi bi-trash"></i>
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
                <div>
                  <ReactPaginate
                    previousLabel="<"
                    nextLabel=">"
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination justify-content-center"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              </Tab>
              <Tab eventKey="add" title={onEdit ? "Edit" : "Add"}>
                <Form>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Promo Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Promo Name"
                      value={promoName}
                      onChange={(e) => setPromoName(e.target.value)}
                    />
                    {/* <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text> */}
                  </Form.Group>

                  <Form.Group className="mb-3 col-12">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add Description"
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Youtube</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Youtube Link"
                      value={youTube}
                      onChange={(e) => setYouTube(e.target.value)}
                    />
                  </Form.Group>

                  {onEdit ? (
                    <Button
                      variant="primary"
                      onClick={(e) => editData(e)}
                      type="submit"
                    >
                      Edit
                    </Button>
                  ) : (
                    <Button
                      variant="primary"
                      onClick={(e) => postData(e)}
                      type="submit"
                    >
                      Submit
                    </Button>
                  )}
                  <Button
                    variant="outline-primary"
                    type="submit"
                    style={{ marginLeft: 15 }}
                    onClick={(e) => setKey(1)}
                  >
                    Cancel
                  </Button>
                </Form>
              </Tab>
            </Tabs>
          </Card.Body>
        </Card>
      </main>
    </>
  );
}
