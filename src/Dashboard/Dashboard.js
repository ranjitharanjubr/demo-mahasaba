import React, { useEffect, useState } from "react";
import axios from "axios";
import { Slide } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";
import BaseUrl from "../BaseUrl";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import PromoCard from "./PromoCard";

const responsive = {
  desktop: {
    breakpoint: { max: 1500, min: 750 },
    items: 3,
    slidesToSlide: 2,
  },
  // tablet: {
  //   breakpoint: { max: 1024, min: 464 },
  //   items: 2,
  //   slidesToSlide: 2,
  // },
  // mobile: {
  //   breakpoint: { max: 464, min: 0 },
  //   items: 1,
  //   slidesToSlide: 1,
  // },
};

export default function Dashboard(props) {
  const [bannerData, setBannerData] = useState([]);
  const [promoData, setPromoData] = useState([]);
  const [countData, setCountData] = useState([]);

  const user = JSON.parse(sessionStorage.getItem("user"));

  const ImageUrl = `${BaseUrl}/file/downloadFile/?filePath=`;
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

  const fetchBannerData = async () => {
    await axios({
      method: "get",
      url: `${BaseUrl}/dashboard/v1/getAllAdvertisementByPagination/{pageNumber}/{pageSize}?pageNumber=0&pageSize=5`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data.content;
        setBannerData(data);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const fetchPromoData = async () => {
    await axios({
      method: "get",
      url: `${BaseUrl}/dashboard/v1/getAllPromoByPagination/{pageNumber}/{pageSize}?pageNumber=0&pageSize=5`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data.content;
        setPromoData(data);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const fetchCountData = async () => {
    await axios({
      method: "get",
      url: `${BaseUrl}/dashboard/v1/adminDashboard`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setCountData(data);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchBannerData();
    fetchCountData();
    fetchPromoData();
  }, []);

  return (
    <>
      <main id="main" className="main ">
        <div className="pagetitle">
          <h1>Dashboard</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="">Home</a>
              </li>
              <li className="breadcrumb-item active">Dashboard</li>
            </ol>
          </nav>
        </div>
        <div className="section dashboard m-2">
          <div className="card">
            <div className="slide-container">
              <Slide arrows={false}>
                {bannerData.map((data, index) => (
                  <div className="each-slide" key={index}>
                    {/* <div>
                      <img
                        style={{ width: "100%", height: 270 }}
                        src={ImageUrl + encodeURIComponent(data.filePath)}
                      />
                    </div> */}
                  </div>
                ))}
              </Slide>
            </div>
          </div>
        </div>
        {/* <div className="info-card section dashboard m-2">
          {catData.map((data, index) => (
            <Button variant="primary" className="m-2">
              {data.categoryName}
            </Button>
          ))}
        </div> */}
        <section class="section dashboard">
          <div class="col-lg-12">
            <div class="row">
              <div class="col-xxl-4 col-md-2">
                <div class="card info-card sales-card">
                  <div class="card-body">
                    <h5 class="card-title">Promo</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-tv"></i>
                      </div>
                      <div class="ps-3">
                        <h6>{countData.promoCount}</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xxl-4 col-md-2">
                <div class="card info-card revenue-card">
                  <div class="card-body">
                    <h5 class="card-title">Course</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-book"></i>
                      </div>
                      <div class="ps-3">
                        <h6>{countData.courseCount}</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xxl-4 col-md-2">
                <div class="card info-card customers-card">
                  <div class="card-body">
                    <h5 class="card-title">Category</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-card-list"></i>
                      </div>
                      <div class="ps-3">
                        <h6>{countData.categoryCount}</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xxl-4 col-md-2">
                <div class="card info-card language-card">
                  <div class="card-body">
                    <h5 class="card-title">Language</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-translate"></i>
                      </div>
                      <div class="ps-3">
                        <h6>{countData.languageCount}</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xxl-4 col-md-2">
                <div class="card info-card user-card">
                  <div class="card-body">
                    <h5 class="card-title">Users</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-people-fill"></i>
                      </div>
                      <div class="ps-3">
                        <h6>{countData.userProfileCount}</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xxl-4 col-md-2">
                <div class="card info-card sales-card">
                  <div class="card-body">
                    <h5 class="card-title">Banner</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-badge-ad-fill"></i>
                      </div>
                      <div class="ps-3">
                        <h6>{countData.bannerCount}</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <div className="slide-container">
          <Carousel
            swipeable={false}
            draggable={false}
            responsive={responsive}
            ssr={true}
            infinite={true}
            autoPlay={props.deviceType !== "mobile" ? true : false}
            autoPlaySpeed={3000}
            keyBoardControl={true}
            customTransition="all .5"
            transitionDuration={500}
            containerClass="carousel-container"
            removeArrowOnDeviceType={["tablet", "mobile"]}
            deviceType={props.deviceType}
            dotListClass="custom-dot-list-style"
            itemClass="carousel-item-padding-40-px"
          >
            {promoData.map((data, index) => {
              return (
                <div className="each-slide mt-4" key={index}>
                  {/* <PromoCard youTube={data.youTube} /> */}
                </div>
              );
            })}
          </Carousel>
        </div>
      </main>
    </>
  );
}
