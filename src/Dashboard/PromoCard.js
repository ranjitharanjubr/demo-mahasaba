import React from "react";
import { Card } from "react-bootstrap";

export default function PromoCard({ youTube }) {
  return (
    <div>
      <Card
        style={{
          height: "13rem",
          width: "20rem",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Card.Body>
          <iframe
            width="300"
            height="150"
            src={`https://www.youtube.com/embed/${youTube}`}
            frameBorder="0"
            allow="autoplay;"
            allowFullScreen
            title="Embedded youtube"
          />
        </Card.Body>
      </Card>
    </div>
  );
}
