import React, { useState } from "react";
import { Modal, Row, Col } from "react-bootstrap";
import { useNavigate, Link } from "react-router-dom";
import BaseUrl from "../../BaseUrl";
import logo from "../../assets/images/logo.png";
import "./MyModal.css";
import axios from "axios";

export default function AuthModal(props) {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [otp, setOtp] = useState("");
  const [loginOtp, setLoginOtp] = useState(false);
  const [onForgot, setOnForgot] = useState(false);

  const handleLogin = (e) => {
    e.preventDefault();
    axios({
      method: "post",
      url: `${BaseUrl}/login/v1/userLogin`,
      ///login/v1/userLogin`
      headers: {
        "Content-type": "application/json",
      },
      data: {
        password,
        userName,
      },
    })
      .then((response) => {
        if (response.data.accessToken) {
          sessionStorage.setItem("user", JSON.stringify(response.data));
          authUser();
        } else {
          alert(response.data.errorMessage);
        }
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
    setUserName("");
    setPassword("");
  };

  const navigate = useNavigate();

  function authUser() {
    const user = JSON.parse(sessionStorage.getItem("user"));
    navigate("/Admin");
    // return { Authorization: "Bearer " + user.accessToken };
  }

  const handleButtonChange = (e) => {
    e.preventDefault();
    setLoginOtp(!loginOtp);
  };

  const handleOtp = (e, userName) => {
    e.preventDefault();
    axios({
      method: "get",
      url: `${BaseUrl}/login/v1/forgotPassword/${userName}`,
      //login/v1/forgotPassword/${userName}`
      data: {
        userName,
      },
    })
      .then((res) => {
        const data = res.data;
        if (data.responseCode === 1000) {
          alert(data.message);
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleReset = (e) => {
    e.preventDefault();
    axios({
      method: "post",
      url: `${BaseUrl}/login/v1/resetPassword`,
      data: {
        userName,
        newPassword,
        otp,
      },
    })
      .then((res) => {
        const data = res.data;
        console.log(data);
        if (data.responseCode === 202) {
          alert(data.message);
        } else if (data.responseCode === 403) {
          alert(data.message);
        }
        setOnForgot(false);
      })
      .catch((error) => {
        console.log(error);
      });
    setPassword("");
    setNewPassword("");
    setOtp("");
  };

  return (
    <div className="out-container">
      <Modal {...props} size="lg" centered>
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Welcome to Mahaasabha
          </Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ marginBottom: 40 }}>
          <Row>
            <Col
              xs={6}
              md={4}
              lg={4}
              className="d-flex align-items-center justify-content-center"
            >
              {/* <img
                src={logo}
                style={{ width: 350, height: 300, marginLeft: 50 }}
                alt="logo"
              /> */}
            </Col>
            <Col
              xs={6}
              md={4}
              lg={8}
              className="d-flex align-items-center justify-content-center"
            >
              <div className={onForgot ? "HidePassword" : "container"}>
                <div className="card-body">
                  <div className="">
                    <h5 className="card-title text-center pb-0 fs-4">Login</h5>
                  </div>
                  <div style={{ marginLeft: 50 }}>
                    <form className="row g-3">
                      <div className="col-12">
                        <label className="form-label">
                          Username/E-mail/PhoneNumber
                        </label>
                        <div className="input-group ">
                          {/* <span className="input-group-text">+91</span> */}
                          <input
                            type="text"
                            placeholder="Enter Your Username"
                            name="userName"
                            className="form-control"
                            value={userName}
                            onChange={(e) => setUserName(e.target.value)}
                            required
                          />
                        </div>
                      </div>

                      <div
                        className="col-12"
                        className={loginOtp ? "HidePassword" : "ShowPassword"}
                      >
                        <label className="form-label">Password</label>
                        <input
                          type="password"
                          placeholder="Enter Your Password"
                          name="passwword"
                          className="form-control"
                          value={password}
                          onChange={(e) => setPassword(e.target.value)}
                          required
                        />
                      </div>

                      <div
                        className="col-12"
                        className={loginOtp ? "ShowPassword" : "HidePassword"}
                      >
                        <label className="form-label">OTP</label>
                        <input
                          type="password"
                          placeholder="Enter OTP here"
                          name="otp"
                          className="form-control"
                          value={otp}
                          onChange={(e) => setOtp(e.target.value)}
                          required
                        />
                      </div>
                      <div className="col-12">
                        <button
                          className="btn btn-primary w-100"
                          type="submit"
                          onClick={(e) => handleLogin(e)}
                        >
                          Login
                        </button>
                      </div>
                      <div className="col-12">
                        <button
                          className="btn btn-outline-primary w-100"
                          type="submit"
                          onClick={(e) => handleButtonChange(e)}
                        >
                          {loginOtp ? "Login With Password" : "Login with OTP"}
                        </button>
                      </div>
                      <div className="col-12">
                        <p className="small mb-0">
                          <a
                            style={{ cursor: "pointer" }}
                            onClick={(e) => setOnForgot(true)}
                          >
                            Forgot Password?
                          </a>
                        </p>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div className={onForgot ? "container" : "HidePassword"}>
                <div className="card-body">
                  <div className="">
                    <h5 className="card-title text-center pb-0 fs-4">
                      Forgot Password
                    </h5>
                  </div>
                  <div style={{ marginLeft: 50 }}>
                    <form className="row g-3">
                      <div className="col-12">
                        <label className="form-label">
                          Username/E-mail/PhoneNumber
                        </label>
                        <div className="input-group ">
                          {/* <span className="input-group-text">+91</span> */}
                          <input
                            type="text"
                            placeholder="Enter Your Username"
                            name="userName"
                            className="form-control"
                            value={userName}
                            onChange={(e) => setUserName(e.target.value)}
                          />
                        </div>
                      </div>

                      <div className="col-12" className="ShowPassword">
                        <label className="form-label">New Password</label>
                        <input
                          type="password"
                          placeholder="Enter New Password"
                          name="new-password"
                          className="form-control"
                          value={newPassword}
                          onChange={(e) => setNewPassword(e.target.value)}
                        />
                      </div>
                      <div className="showPassword">
                        <Row>
                          <Col>
                            <label className="form-label">OTP</label>
                            <input
                              type="password"
                              placeholder="Enter OTP here"
                              name="otp"
                              className="form-control"
                              value={otp}
                              onChange={(e) => setOtp(e.target.value)}
                            />
                          </Col>
                          <Col style={{ marginTop: 32 }}>
                            <button
                              className="btn btn-outline-primary"
                              type="submit"
                              onClick={(e) => handleOtp(e, userName)}
                            >
                              Send OTP
                            </button>
                          </Col>
                        </Row>
                      </div>
                      <div className="col-12">
                        <button
                          className="btn btn-primary w-100"
                          type="submit"
                          onClick={(e) => handleReset(e)}
                        >
                          Reset Password
                        </button>
                      </div>

                      <div className="col-12">
                        <p className="small mb-0">
                          <a
                            style={{ cursor: "pointer" }}
                            onClick={(e) => setOnForgot(false)}
                          >
                            Go to Login
                          </a>
                        </p>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Modal.Body>
      </Modal>
    </div>
  );
}
