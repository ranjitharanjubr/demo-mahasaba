import React, { useState, useEffect } from "react";
import { Table, Form, Button, Tabs, Tab, Row, Col, FormGroup, FormControl } from "react-bootstrap";
import Moment from "moment";
import axios from "axios";
import { Link } from "react-router-dom";
import { Card } from "react-bootstrap";
import ReactPaginate from "react-paginate";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import validator from 'validator';


export default function Membership() {
  const [key, setKey] = useState("list-view");
  const [onEdit, setOnEdit] = useState(false);
  const [membership, setMembership] = useState([]);
  const [fullName, setFullName] = useState("");
  const [membershipId, setMembershipId] = useState("");
  const [description, setDescription] = useState("");
  const [membershipCode, setMembershipCode] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [isError, setIsError] = useState(false);

  //const [emailId, setEmailId] = useState("");
  const [gender, setGender] = useState("");
  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);
  const [emailId, setEmailId] = useState('')
  
  const validateEmailId = (e) => {
    var emailId = e.target.value
  
    if (validator.isEmail(emailId)) {
      setEmailError('Valid Email :)')
    } else {
      setEmailError('Enter valid Email!')
    }}

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };
  const BaseUrl = "https://executivetracking.cloudjiffy.net/Mahaasabha";

 

  let data = {
    createdBy: {
      userId: user.userId,
    },
    description,
    membershipCode,
    mobileNumber,
    emailId,
    gender,


    fullName,
  };

  const postData = (e) => {
    e.preventDefault();
    axios({
      method: "post",
      url: `${BaseUrl}/membership/v1/createMembership`,
      headers,
      data,
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setFullName("");

    setDescription("");
    setMembershipCode("");
    setMobileNumber("");
    setEmailId("");
    setGender("");

    setKey("list-view");
    FetchData();
  };

  const FetchData = async () => {
    await axios({
      method: "get",
      url: `https://executivetracking.cloudjiffy.net/Mahaasabha/membership/v1/getAllMembershipByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
     // url: `${BaseUrl}/membership/v1/getAllMembershipByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,

      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setMembership(data.content);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    FetchData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    FetchData(pageSize);
  };
  const handleChangee = (e) => {
    const target = e.target;
    if (target.checked) {
      setGender(target.value);
    }
 };

  useEffect(() => {
    FetchData();
  }, [
    description,
    membershipCode,
    mobileNumber,
    emailId,
    gender,

   
    fullName,

    pageSize,
    pageNumber,
  ]);

  const handleRemove = (membershipId) => {
    axios({
      method: "delete",
      url: `${BaseUrl}/membership/v1/deleteMembershipById/${membershipId}`,
      headers,
    })
      .then((res) => {
        if (res.data.responseCode === 200) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
        FetchData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleEdit = (membershipId) => {
    axios({
      method: "get",
      url: `${BaseUrl}/membership/v1/getMembershipByMembershipId/{membershipId}?membershipId=${membershipId}`,
      headers,
    })
      .then(function (res) {
        setFullName(res.data.fullName);
        setEmailId(res.data.emailId);
        setGender(res.data.gender);
        setDescription(res.data.description);
        setMembershipCode(res.data.membershipCode);

        setMobileNumber(res.data.mobileNumber);

        setOnEdit(true);
        setKey("add");
        setMembershipId(membershipId);
        FetchData();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const editData = (e) => {
    e.preventDefault();
    axios({
      method: "put",
      url: `${BaseUrl}/membership/v1/updateMembership`,
      headers,
      data,
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setFullName("");
    setEmailId("");
    setGender("");
    setDescription("");
    setMembershipCode("");

    setMobileNumber("");

    setKey("list-view");
    FetchData();
  };

  const truncate = (str) => {
    return str.length > 20 ? str.substring(0, 15) + "..." : str;
  };

  return (
    <>
      <main id="main" className="main">
        <div className={key === "add" ? "pagetitle mb-5" : "pagetitle"}>
          <h1>Membership</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/Admin">Home</Link>
              </li>
              <li className="breadcrumb-item active">Membership</li>
            </ol>
          </nav>
        </div>
        <div className={key === "list-view" ? "displayForm mt-3" : "hideForm"}>
          <select
            value={value}
            onChange={handleChange}
            style={{ borderColor: "powderblue", borderRadius: 3, padding: 2 }}
          >
            <option value="10">10 / Pages</option>
            <option value="25">25 / Pages</option>
            <option value="50">50 / Pages</option>
            <option value="100">100 / Pages</option>
          </select>
        </div>
        <Card style={{ marginTop: 10 }}>
          <Card.Body>
            <Tabs
              activeKey={key}
              onSelect={(key) => setKey(key)}
              className="mt-3"
            >
              <Tab eventKey="list-view" title="List View">
                <Table striped bordered hover responsive>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th style={{ minWidth: 125 }}>Full Name</th>
                      
                      <th style={{ minWidth: 125 }}>Gender</th>
                      <th style={{ minWidth: 125 }}> EmailId</th>
                      <th style={{ minWidth: 150 }}>Description</th>
                      <th style={{ minWidth: 125 }}>membershipCode</th>
                      <th style={{ minWidth: 125 }}>Mobile Number</th>

                      <th style={{ minWidth: 125 }}>Created By</th>
                      <th style={{ minWidth: 125 }}>Updated By</th>
                      <th style={{ minWidth: 125 }}>Inserted Date</th>
                      <th style={{ minWidth: 125 }}>Updated Date</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {membership.map((data, index) => {
                      return (
                        <tr key={data.membershipId}>
                          <th scope="row">{index + 1}</th>
                          <td>{data.fullName}</td>
                          <td>{data.emailId}</td>
                          <td>{data.gender}</td>
                          <td>{truncate(data.description)}</td>
                          <td>{data.membershipCode}</td>

                          <td>{data.mobileNumber}</td>

                          <td>{}</td>
                          <td>{}</td>

                          {/* <td>{data.createdBy.userName}</td>
                          <td>{data.updatedBy.userName}</td> */}
                          <td>
                            {Moment(data.insertedDate).format("DD-MM-YYYY")}
                          </td>
                          <td>
                            {Moment(data.updatedDate).format("DD-MM-YYYY")}
                          </td>
                          <td className="inline-block">
                            <button
                              type="button"
                              className="btn btn-icon btn-sm"
                              title="Edit"
                              onClick={(e) => handleEdit(data.membershipId, e)}
                            >
                              <i class="bi bi-pencil-square"></i>
                            </button>
                            <button
                              type="button"
                              className="btn btn-icon btn-sm js-sweetalert"
                              title="Delete"
                              onClick={(e) =>
                                handleRemove(data.membershipId, e)
                              }
                            >
                              <i class="bi bi-trash"></i>
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
                <div>
                  <ReactPaginate
                    previousLabel="<"
                    nextLabel=">"
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination justify-content-center"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              </Tab>
              <Tab eventKey="add" title={onEdit ? "Edit" : "Add"}>
                <Form>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Full Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Full Name"
                      value={fullName}
                      onChange={(e) => setFullName(e.target.value)}
                    />
                  </Form.Group>
                  {/* <Form.Group className="mb-3 col-6">
                    <Form.Label>EmailId</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add emailId"
                      value={emailId}
                      onChange={(e) => setEmailId(e.target.value)}   onChange={(e) => validateEmailId(e)}>
                                       onClick={emailValidate(customerEmail)}
                    />
                  </Form.Group> */}

<div style={{
      margin: 'auto',
      marginLeft: '1px',
    }}>
      <pre>
        <h6>Enter Email: </h6><input type="email" id="userEmail" placeholder="Add valid emailId"
        onChange={(e) => validateEmailId(e)}></input> <br />
        <span style={{
          fontWeight: 'bold',
          color: 'red',
        }}>{emailId}</span>
      </pre>
    </div>





                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Gender</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add gender"
                      value={gender}
                      onChange={(e) => setGender(e.target.value)}
                    />
                  </Form.Group>


      {/* <div>
      <Form.Label>Gender</Form.Label>
        <label>
        <h6>Male</h6>
          <input type="radio" value="male"  placeholder="Add gender" checked={gender == 'male'} onChange={handleChangee} />
          
        </label>
        <label>
        <h6>Female</h6>
          <input type="radio"  value="female" checked={gender == 'female'} onChange={handleChangee} />
          
        </label>
      </div>
      <button type="submit">Submit</button> */}
    






                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Description"
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Membership Code</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Membership Code"
                      value={membershipCode}
                      onChange={(e) => setMembershipCode(e.target.value)}
                    />
                  </Form.Group>

                  {/* <Form.Group className="mb-3 col-6">
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Mobile Number"
                      value={mobileNumber}
                      onChange={(e) => setMobileNumber(e.target.value)}
                    />
                  </Form.Group> */}

<div
    
    >
      {/* <Form.Group className="mb-3 col-6"> */}
      <Form.Label>MobileNumber</Form.Label>
      <TextField
      placeholder="Add mobileNumber"
        type="tel"
        error={isError}
        value={mobileNumber}
        // label="Enter Phone Number"
        
        onChange={(e) => {
          setMobileNumber(e.target.value );
          if (e.target.value.length > 10) {
            setIsError(true);
          }
        }}
        InputProps={{
          startAdornment: <InputAdornment position="start">
            <h6>   +91{mobileNumber}</h6> 
             </InputAdornment>,
        }}
      />
      {/* <h3>{mobileNumber} </h3> */}
      {/* </Form.Group> */}
    </div>

                  <Row className="align-items-center">
                    <Col>
                      {/* <Form.Group controlId="formFile" className="mb-3">
                        <Form.Label>Select Photo</Form.Label>
                        <Form.Control
                          type="file"
                          onChange={(e) => onFileChange(e)}
                        />
                      </Form.Group> */}
                    </Col>
                    {/* <Col className="pt-3">
                      <Button
                        variant="primary"
                        onClick={(e) => onFileUpload(e)}
                        type="submit"
                      >
                        Upload
                      </Button>
                    </Col> */}
                  </Row>

                  {onEdit ? (
                    <Button
                      variant="primary"
                      onClick={(e) => editData(e)}
                      type="submit"
                    >
                      Edit
                    </Button>
                  ) : (
                    <Button
                      variant="primary"
                      onClick={(e) => postData(e)}
                      type="submit"
                    >
                      Submit
                    </Button>
                  )}
                  <Button
                    variant="outline-primary"
                    type="submit"
                    style={{ marginLeft: 15 }}
                    onClick={(e) => setKey(1)}
                  >
                    Cancel
                  </Button>
                </Form>
              </Tab>
            </Tabs>
          </Card.Body>
        </Card>
      </main>
    </>
  );
}
