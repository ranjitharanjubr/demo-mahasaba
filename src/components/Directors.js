import React, { useState, useEffect } from "react";
import { Table, Form, Button, Tabs, Tab, Row, Col } from "react-bootstrap";
import Moment from "moment";
import axios from "axios";
//import BaseUrl from "../../BaseUrl";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import ReactPaginate from "react-paginate";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import validator from 'validator';

export default function Directors() {
  const [key, setKey] = useState("list-view");
  const [fullName, setFullName] = useState("");
  const [gender, setGender] = useState("");
  const [director, setDirector] = useState([]);
  const [description, setDescription] = useState("");
  const [emailId, setEmailId] = useState("");
  const [profilePicName,setProfilePicName] = useState("");
  const [profilePicPath,setProfilePicPath] = useState("");
  const [isError, setIsError] = useState(false);

  const [mobileNumber, setMobileNumber] = useState("");
  const [alternativeMobileNumber, setAlternativeMobileNumber] = useState("");
  const [onEdit, setOnEdit] = useState(false);
  const [directorId, setDirectorId] = useState("");
  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);
  //const [fileName, setFileName] = useState("");
  const [selectedFile, setselectedFile] = useState(null);
  const validateEmailId = (e) => {
    var emailId = e.target.value
  
    if (validator.isEmail(emailId)) {
      setEmailError('Valid Email :)')
    } else {
      setEmailError('Enter valid Email!')
    }}

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };
  const BaseUrl = "https://executivetracking.cloudjiffy.net/Mahaasabha";

  const profilePicNameUrl = `https://executivetracking.cloudjiffy.net/Mahaasabha/file/downloadFile/?filePath=`;

  const postData = (e) => {
    e.preventDefault();
    axios({
      method: "post",
      url: `${BaseUrl}/director/v1/createDirector`,
      headers,
      data: {
        fullName,
        gender,
        createdBy: {
          userId: user.userId,
        },
        description,
        emailId,
        profilePicName,
        profilePicPath,
        mobileNumber,
        alternativeMobileNumber,
        //fileName,
      },
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setFullName("");
    setGender("");
    setDescription("");
    setEmailId("");
    setProfilePicName("");
    setProfilePicPath("");
    setMobileNumber("");
    setAlternativeMobileNumber("");
    //setselectedFile(null);
    //setFileName("");
    setKey("list-view");
    FetchData();
  };

  const FetchData = async () => {
    await axios({
      method: "get",
      url: `${BaseUrl}/director/v1/getAllDirectorByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setDirector(data.content);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    FetchData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    FetchData(pageSize);
  };

  useEffect(() => {
    FetchData();
  }, [
    fullName,
    gender,
    description,
    emailId,
    profilePicName,
    profilePicPath,
    mobileNumber,
    alternativeMobileNumber,
    pageSize,
    pageNumber,
  ]);

  const handleRemove = (directorId) => {
    axios({
      method: "delete",
      url: `${BaseUrl}/director/v1/deleteDirectorByDirectorId/{directorId}?directorId=${directorId}`,
      headers,
    })
      .then((res) => {
        if (res.data.responseCode === 200) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
        FetchData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleEdit = (directorId) => {
    axios({
      method: "get",
      url: `${BaseUrl}/director/v1/getDirectorByDirectorId/{directorId}?directorId=${directorId}`,
      headers,
    })
      .then(function (res) {
        setFullName(res.data.fullName);
        setGender(res.data.gender);
        setDescription(res.data.description);
        setEmailId(res.data.emailId);
        setProfilePicName(res.data.profilePicName);
        setProfilePicPath(res.data.profilePicPath);
        setMobileNumber(res.data.mobileNumber);
        setAlternativeMobileNumber(res.data.alternativeMobileNumber);
        //setFileName(res.data.fileName);
        setOnEdit(true);
        setKey("add");
        setDirectorId(directorId);
        FetchData();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const onFileChange = (e) => {
    setselectedFile(e.target.files[0]);
  };

  const onFileUpload = (e) => {
    e.preventDefault();
    const data = new FormData();

    data.append("file", selectedFile);

    axios({
      mode: "no-cors",
      method: "post",
      url: `https://executivetracking.cloudjiffy.net/Mahaasabha/file/uploadFile`,
      headers: {
        "content-type": "multipart/form-data",
        Authorization: "Bearer " + user.accessToken,
      },
      data,
    })
      .then(function (res) {
        setProfilePicName(res.data.profilePicName);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const editData = (e) => {
    e.preventDefault();
    axios({
      method: "put",
      url: `${BaseUrl}/director/v1/updateDirector`,
      headers,
      data: {
        description,
        emailId,
        profilePicName,
        profilePicPath,
        mobileNumber,
        alternativeMobileNumber,
        directorId,
        fullName,
        gender,
        updatedBy: {
          userId: user.userId,
        },
      },
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setFullName("");
    setGender("");
    setDescription("");
    setEmailId("");
    setProfilePicName("");
    setProfilePicPath("");
    setMobileNumber("");
    setAlternativeMobileNumber("");
    //setselectedFile(null);
    //setFileName("");
    setOnEdit(false);
    setKey("list-view");
    FetchData();
  };

  return (
    <>
      <main id="main" className="main">
        <div className={key === "add" ? "pagetitle mb-5" : "pagetitle"}>
          <h1>Director</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/Admin/adsmenu">AdsMenu</Link>
              </li>
              <li className="breadcrumb-item active">Ads</li>
            </ol>
          </nav>
        </div>
        <div className={key === "list-view" ? "displayForm mt-3" : "hideForm"}>
          <select
            value={value}
            onChange={handleChange}
            style={{ borderColor: "powderblue", borderRadius: 3, padding: 2 }}
          >
            <option value="10">10 / Pages</option>
            <option value="25">25 / Pages</option>
            <option value="50">50 / Pages</option>
            <option value="100">100 / Pages</option>
          </select>
        </div>
        <Card style={{ marginTop: 10 }}>
          <Card.Body>
            <Tabs
              activeKey={key}
              onSelect={(key) => setKey(key)}
              className="mt-3"
            >
              <Tab eventKey="list-view" title="List View">
                <Table striped bordered hover scroll responsive>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>ProfilePic</th>
                      <th>Full Name</th>
                      <th>Gender</th>
                      <th>Description</th>
                      <th>EmailId</th>
                      <th>MobileNumber</th>
                      <th>AlternativeMobileNumber</th>

                      <th>Created By</th>
                      <th>Updated By</th>
                      <th>Inserted Date</th>
                      <th>Updated Date</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {director.map((data, index) => {
                      return (
                        <tr key={data.directorId}>
                          <th scope="row">{index + 1}</th>

                          <td>
                            <img
                              src={profilePicNameUrl + data.profilePicPath}
                              alt={data.profilePicName}
                              style={{ width: 100, height: 50 }}
                            />
                          </td>
                          <td>{data.fullName}</td>
                          <td>{data.gender}</td>
                          <td
                            style={{
                              "max-width": "200px",
                              overflow: "hidden",
                              "text-overflow": "ellipsis",
                              height: "56px",
                            }}
                          >
                            {data.description}
                          </td>

                          <td>{data.emailId}</td>
                          <td>{data.mobileNumber}</td>
                          <td>{data.alternativeMobileNumber}</td>

                          {/* <td>{}</td>
                          <td></td> */}
                          <td>{data.createdBy.userName}</td>
                          <td>{data.updatedBy.userName}</td>
                          <td>
                            {Moment(data.insertedDate).format("DD-MM-YYYY")}
                          </td>
                          <td>
                            {Moment(data.updatedDate).format("DD-MM-YYYY")}
                          </td>
                          <td>
                            <button
                              type="button"
                              className="btn btn-icon btn-sm"
                              title="Edit"
                              onClick={(e) => handleEdit(data.directorId, e)}
                            >
                              <i class="bi bi-pencil-square"></i>
                            </button>
                            <button
                              type="button"
                              className="btn btn-icon btn-sm js-sweetalert"
                              title="Delete"
                              onClick={(e) => handleRemove(data.directorId, e)}
                            >
                              <i class="bi bi-trash"></i>
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
                <div>
                  <ReactPaginate
                    previousLabel="<"
                    nextLabel=">"
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination justify-content-center"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              </Tab>
              <Tab eventKey="add" title={onEdit ? "Edit" : "Add"}>
                <Form>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Full Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Full Name"
                      value={fullName}
                      onChange={(e) => setFullName(e.target.value)}
                    />
                    {/* <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text> */}
                  </Form.Group>

                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Gender</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Full Name"
                      value={gender}
                      onChange={(e) => setGender(e.target.value)}
                    />
                    {/* <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text> */}
                  </Form.Group>

                  <Form.Group className="mb-3 col-12">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add Description"
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                    />
                  </Form.Group>

                  {/* <Form.Group className="mb-3 col-12">
                    <Form.Label>EmailId</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add EmailId"
                      value={emailId}
                      onChange={(e) => setEmailId(e.target.value)}
                    />
                  </Form.Group> */}


<div style={{
      margin: 'auto',
      marginLeft: '1px',
    }}>
      <pre>
        <h6>Enter Email: </h6><input type="email" id="userEmail" placeholder="Add valid emailId"
        onChange={(e) => validateEmailId(e)}></input> <br />
        <span style={{
          fontWeight: 'bold',
          color: 'red',
        }}>{emailId}</span>
      </pre>
    </div>















                  {/* <Form.Group className="mb-3 col-12">
                    <Form.Label>MobileNumber</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add MobileNumber"
                      value={mobileNumber}
                      onChange={(e) => setMobileNumber(e.target.value)}
                    />
                  </Form.Group> */}

<div
    
    >
      {/* <Form.Group className="mb-3 col-6"> */}
      <Form.Label>MobileNumber</Form.Label>
      <TextField
      placeholder="Add mobileNumber"
        type="tel"
        error={isError}
        value={mobileNumber}
        // label="Enter Phone Number"
        
        onChange={(e) => {
          setMobileNumber(e.target.value );
          if (e.target.value.length > 10) {
            setIsError(true);
          }
        }}
        InputProps={{
          startAdornment: <InputAdornment position="start">
            <h6>   +91{mobileNumber}</h6> 
             </InputAdornment>,
        }}
      />
      {/* <h3>{mobileNumber} </h3> */}
      {/* </Form.Group> */}
    </div>

                  {/* <Form.Group className="mb-3 col-12">
                    <Form.Label>AlternativeMobileNumber</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add AlternativeMobileNumber"
                      value={alternativeMobileNumber}
                      onChange={(e) =>
                        setAlternativeMobileNumber(e.target.value)
                      }
                    />
                  </Form.Group> */}

<div
    
    >
      {/* <Form.Group className="mb-3 col-6"> */}
      <Form.Label>Alternate MobileNumber</Form.Label>
      <TextField
      placeholder="Add Alternate mobileNumber"
        type="tel"
        error={isError}
        value={mobileNumber}
        // label="Enter Phone Number"
        
        onChange={(e) => {
          setAlternativeMobileNumber(e.target.value );
          if (e.target.value.length > 10) {
            setIsError(true);
          }
        }}
        InputProps={{
          startAdornment: <InputAdornment position="start">
            <h6>   +91{alternativeMobileNumber }</h6> 
             </InputAdornment>,
        }}
      />
      {/* <h3>{mobileNumber} </h3> */}
      {/* </Form.Group> */}
    </div>

                  <Row className="align-items-center">
                    <Col>
                      <Form.Group controlId="formFile" className="mb-3">
                        <Form.Label>Profile Pic</Form.Label>
                        <Form.Control
                          type="file"
                          onChange={(e) => onFileChange(e)}
                        />
                      </Form.Group>
                    </Col>
                    <Col className="pt-3">
                      <Button
                        variant="primary"
                        onClick={(e) => onFileUpload(e)}
                        type="submit"
                      >
                        Upload
                      </Button>
                    </Col>
                  </Row>
                  {onEdit ? (
                    <Button
                      variant="primary"
                      onClick={(e) => editData(e)}
                      type="submit"
                    >
                      Edit
                    </Button>
                  ) : (
                    <Button
                      variant="primary"
                      onClick={(e) => postData(e)}
                      type="submit"
                    >
                      Submit
                    </Button>
                  )}
                  <Button
                    variant="outline-primary"
                    type="submit"
                    style={{ marginLeft: 15 }}
                    onClick={(e) => setKey(1)}
                  >
                    Cancel
                  </Button>
                </Form>
              </Tab>
            </Tabs>
          </Card.Body>
        </Card>
      </main>
    </>
  );
}
