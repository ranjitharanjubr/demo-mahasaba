import React, { useState, useEffect } from "react";

import axios from "axios";
import userimg from "../assets/images/userimg.png";
import logo from "../assets/img/logo.jpg";
import { Link, Outlet, useNavigate } from "react-router-dom";

export default function Layout() {
  const [userInfo, setUserInfo] = useState({});
  const [sidebarVisible, setSidebarVisible] = useState(false);

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };
  const baseUrl =
    "https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/login/v1";

  const fetchUser = () => {
    axios({
      method: "get",
      url: `${baseUrl}/queryUserProfileByUserName/${user.userName}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setUserInfo(data);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchUser();
  }, []);

  let navigate = useNavigate();
  const Logout = () => {
    navigate("/Admin/login");
    sessionStorage.removeItem("user");
  };

  let date = new Date();
  let year = date.getFullYear();
  return (
    <>
      <div>
        <header
          id="header"
          className="header fixed-top d-flex align-items-center"
        >
          <div className="d-flex align-items-center justify-content-between">
            <Link to="/Admin" className="logo d-flex align-items-center">
              {/* <img src={logo} alt="" /> */}
              <span className="d-none d-lg-block">Mahaasabha</span>
            </Link>
            <i
              className="bi bi-list toggle-sidebar-btn"
              onClick={(e) => setSidebarVisible(!sidebarVisible)}
            ></i>
          </div>

          <div className="search-bar">
            <form
              className="search-form d-flex align-items-center"
              method="POST"
              action="#"
            >
              <input
                type="text"
                name="query"
                placeholder="Search"
                title="Enter search keyword"
              />
              <button type="submit" title="Search">
                <i className="bi bi-search"></i>
              </button>
            </form>
          </div>

          <nav className="header-nav ms-auto">
            <ul className="d-flex align-items-center">
              <li className="nav-item d-block d-lg-none">
                <a className="nav-link nav-icon search-bar-toggle" href="">
                  <i className="bi bi-search"></i>
                </a>
              </li>

              <li className="nav-item dropdown">
                <Link className="nav-link nav-icon" to="/Admin/news">
                  <i class="bi bi-newspaper"></i>
                  <span className="badge bg-success badge-number">3</span>
                </Link>
              </li>

              <li className="nav-item dropdown">
                <Link className="nav-link nav-icon" to="/Admin/successStory">
                  <i class="bi bi-check-square"></i>
                  {/* <span className="badge bg-success badge-number">3</span> */}
                </Link>
              </li>

              <li className="nav-item dropdown">
                <Link className="nav-link nav-icon" to="/Admin/notification">
                  <i className="bi bi-bell"></i>
                  <span className="badge bg-primary badge-number">4</span>
                </Link>
              </li>

              <li className="nav-item dropdown">
                <Link className="nav-link nav-icon" to="/Admin/store">
                  <i class="bi bi-cart"></i>
                  <span className="badge bg-danger badge-number">3</span>
                </Link>
              </li>

              <li className="nav-item dropdown pe-3 m-3">
                <a
                  className="nav-link nav-profile d-flex align-items-center pe-0"
                  href="#"
                  data-bs-toggle="dropdown"
                >
                  {/* <img
                    src={userimg}
                    alt="Profile"
                    style={{ width: 30, height: 30, marginRight: 8 }}
                  /> */}
                  <i
                    class="bi bi-person-circle nav-icon"
                    style={{ marginRight: 10 }}
                  ></i>
                  <span className="d-none d-md-block dropdown-toggle">
                    {userInfo.userName}
                  </span>
                </a>

                <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
                  <li className="dropdown-header">
                    <h6>{userInfo.userName}</h6>
                    <span>Web Designer</span>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>

                  <li>
                    <Link
                      className="dropdown-item d-flex align-items-center"
                      to="/Admin/user"
                    >
                      <i className="bi bi-person"></i>
                      <span>My Profile</span>
                    </Link>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>

                  <li>
                    <Link
                      className="dropdown-item d-flex align-items-center"
                      to="/Admin/settings"
                    >
                      <i className="bi bi-gear"></i>
                      <span>Account Settings</span>
                    </Link>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>

                  <li>
                    <a
                      className="dropdown-item d-flex align-items-center"
                      href="https://www.bitstreamio.com/"
                      target="_blank"
                    >
                      <i className="bi bi-question-circle"></i>
                      <span>Need Help?</span>
                    </a>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>

                  <li>
                    <a
                      className="dropdown-item d-flex align-items-center"
                      onClick={(e) => Logout(e)}
                    >
                      <i className="bi bi-box-arrow-right"></i>
                      <span>Sign Out</span>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </nav>
        </header>
      </div>
      <div className={sidebarVisible ? " " : "toggle-sidebar"}>
        <aside id="sidebar" className="sidebar">
          <nav>
            <ul className="sidebar-nav" id="sidebar-nav">
              <li className="nav-item">
                <Link className="nav-link " to="/Admin">
                  <i class="bi bi-bookmark"></i>
                  Dashboard
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/Admin/Directors" className="nav-link collapsed">
                  <i className="bi bi-shop"></i>
                  <span>Directors</span>
                </Link>
              </li>

              <li className="nav-item">
                <Link to="/Admin/Membership" className="nav-link collapsed">
                  <i class="bi bi-book"></i>
                  <span>Membership</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/Admin/adsmenu" className="nav-link collapsed">
                  <i className="bi bi-tv"></i>
                  <span>Advertisement</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/Admin/successStory" className="nav-link collapsed">
                  <i className="bi bi-check-square"></i>
                  <span>Success Story</span>
                </Link>
              </li>

              <li className="nav-item">
                <Link to="/Admin/news" className="nav-link collapsed">
                  <i className="bi bi-newspaper"></i>
                  <span>News</span>
                </Link>
              </li>

              <li className="nav-item">
                <Link to="/Admin/Expenditure" className="nav-link collapsed">
                  <i class="bi bi-card-list"></i>
                  Expenditure
                </Link>
              </li>

            

              <li className="nav-item">
                <Link to="/Admin/notification" className="nav-link collapsed">
                  <i class="bi bi-bell"></i>
                  <span>Notification</span>
                </Link>
              </li>

              <li className="nav-item">
                <Link to="/Admin/masters" className="nav-link collapsed">
                  <i className="bi bi-award"></i>
                  <span>Masters</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/Admin/Members" className="nav-link collapsed">
                  <i class="bi bi-person-square"></i>
                  <span>Members</span>
                </Link>
              </li>
              
             
              {/* <li className="nav-item">
                <Link to="/Admin/adsmenu" className="nav-link collapsed">
                  <i className="bi bi-tv"></i>
                  <span>Customers</span>
                </Link>
              </li> */}

              {/* <li className="nav-item">
                <Link to="/Admin/news" className="nav-link collapsed">
                  <i className="bi bi-newspaper"></i>
                  <span>Employees</span>
                </Link>
              </li> */}


            

              {/* <li className="nav-item">
                <Link to="/Admin/successStory" className="nav-link collapsed">
                  <i className="bi bi-check-square"></i>
                  <span>Leave</span>
                </Link>
              </li> */}

              {/* <li className="nav-item">
                <Link to="/Admin/payments" className="nav-link collapsed">
                  <i class="bi bi-credit-card"></i>
                  <span>Payments</span>
                </Link>
              </li> */}

             
             

              {/* <li className="nav-item">
                <Link to="/Admin/user" className="nav-link collapsed">
                  <i class="bi bi-person-square"></i>
                  <span>Holidays</span>
                </Link>
              </li> */}
            
              <li className="nav-item">
                <Link to="/Admin/user" className="nav-link collapsed">
                  <i class="bi bi-person-square"></i>
                  <span>User Management</span>
                </Link>
              </li>
              
           


              
              {/* <li className="nav-item">
                <Link to="/Admin/timetable" className="nav-link collapsed">
                  <i class="bi bi-table"></i>
                  <span>Time Table</span>
                </Link>
              </li> */}
              <li className="nav-item">
                <Link to="/Admin/settings" className="nav-link collapsed">
                  <i class="bi bi-gear"></i>
                  <span>Settings</span>
                </Link>
              </li>

              <li className="nav-item">
                <a
                  // to="/login"
                  onClick={(e) => Logout(e)}
                  className="nav-link collapsed"
                >
                  <i class="bi bi-box-arrow-right"></i>
                  <span>Sign Out</span>
                </a>
              </li>

              {/* <li className="nav-heading">Pages</li>

          <li className="nav-item">
            <a className="nav-link collapsed" href="users-profile.html">
              <i className="bi bi-person"></i>
              <span>Profile</span>
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link collapsed" href="pages-faq.html">
              <i className="bi bi-question-circle"></i>
              <span>F.A.Q</span>
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link collapsed" href="pages-contact.html">
              <i className="bi bi-envelope"></i>
              <span>Contact</span>
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link collapsed" href="pages-register.html">
              <i className="bi bi-card-list"></i>
              <span>Register</span>
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link collapsed" href="pages-login.html">
              <i className="bi bi-box-arrow-in-right"></i>
              <span>Login</span>
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link collapsed" href="pages-error-404.html">
              <i className="bi bi-dash-circle"></i>
              <span>Error 404</span>
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link collapsed" href="pages-blank.html">
              <i className="bi bi-file-earmark"></i>
              <span>Blank</span>
            </a>
          </li> */}
            </ul>
          </nav>
        </aside>
        <Outlet />
      </div>
      <div style={{ marginTop: 50 }}>
        <footer id="footer" class="footer">
          <div class="copyright">
            &copy; {year} Copyright{" "}
            <strong>
              <span>Mahasabha</span>
            </strong>
            . All Rights Reserved
          </div>
          <div class="credits">
            Designed by{" "}
            <a href="https://walkinsoftwares.com/" target="blank">
              Walkin Software Technologies
            </a>
          </div>
        </footer>
      </div>
    </>
  );
}
