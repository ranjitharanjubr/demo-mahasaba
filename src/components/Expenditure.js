import React, { useState, useEffect } from "react";
import { Table, Form, Button, Tabs, Tab } from "react-bootstrap";
import Moment from "moment";
import axios from "axios";
import { Link, useNavigate } from "react-router-dom";
import { Card } from "react-bootstrap";
// import BaseUrl from "../../BaseUrl";
import ReactPaginate from "react-paginate";
import { toast } from "react-toastify";
import { confirmAlert } from 'react-confirm-alert'; // Import


export default function Expenditure() {
    const navigate=useNavigate();
  const [key, setKey] = useState("list-view");
  const [expenditureDate, setExpenditureDate] = useState("");
  const [expenditure, setExpenditure] = useState([]);
  const [description, setDescription] = useState("");
  const [expenditureHeader,setExpenditureHeader] = ("");
  const [expenditureAmount,setExpenditureAmount] = useState("");
  const [fileName, setFileName] = useState("");
  const [onEdit, setOnEdit] = useState(false);
  const [expenditureId, setExpenditureId] = useState("");
  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };
  const baseUrl="https://executivetracking.cloudjiffy.net/Mahaasabha"

  const postData = (e) => {
    e.preventDefault();
    console.log(expenditureDate, description , expenditureHeader, expenditureAmount ,fileName);
    axios({
      method: "post",
      url: `https://executivetracking.cloudjiffy.net/Mahaasabha/expenditure/v1/createExpenditure`,
      headers,
      data: {
        createdBy: {
          userId: user.userId,
        },
        expenditureDate,
        description,
        expenditureHeader,
        expenditureAmount,
        fileName,
      },
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
        //   alert(res.data.message);
        toast.success(res.data.message)
         window.location.reload();
       // navigate("/Admin/Expenditure");
        } else if (res.data.responseCode === 400) {
          toast.error(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setExpenditureDate("");
    setDescription("");
    setExpenditureHeader("");
    setExpenditureAmount("");
    setFileName("");
    setKey("list-view");
    FetchData();
  };

  const FetchData = async () => {
    await axios({
      method: "get",
      url: `${baseUrl}/expenditure/v1/getAllExpenditureByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers, 
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setExpenditure(data.content);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    FetchData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    FetchData(pageSize);
  };

  useEffect(() => {
    FetchData();
  }, [expenditureDate, description,expenditureHeader, expenditureAmount,fileName, pageSize, pageNumber]);

  const handleRemove = (expenditureId) => {
    confirmAlert({
        title: 'Confirm to Delete',
        message: 'Are you sure to do this.',
        buttons: [
          {
            label: 'Yes',
            onClick: () => { axios({
                method: "delete",
                url: `${baseUrl}/expenditure/v1/deleteExpenditureByExpenditureId/${expenditureId}`,
                headers,
              })
                .then((res) => {
                  if (res.data.responseCode === 200) {
                    //alert(res.data.message);
                    var mes=res.data.message
                    toast.error(mes.toUpperCase() );
                  } else if (res.data.responseCode === 400) {
                    toast.error(res.data.errorMessage);
                  }
                  FetchData();
                })
                .catch((err) => {
                  console.log(err);
                });}
          },
          {
            label: 'No',
            onClick: () => {toast.warning("Content NOT DELETED")}
          }
        ]
      });
   
  };

  const handleEdit = (expenditureId) => {
    axios({
      method: "get",
      url: `https://executivetracking.cloudjiffy.net/Mahaasabha/expenditure/v1/getExpenditureByExpenditureId/{expenditureId}?expenditureId=${expenditureId}`,
      headers, 
    })
      .then(function (res) {
        setExpenditureDate(res.data.expenditureDate);
        setDescription(res.data.description);
        setExpenditureHeader(res.data.expenditureHeader);
        setExpenditureAmount(res.data.expenditureAmount);
        setFileName(res.data.fileName);
        setOnEdit(true);
        setKey("add");
        setExpenditureId(expenditureId);
        FetchData();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const editData = (e) => {
    e.preventDefault();
    axios({
      method: "put",
      url: `${baseUrl}/expenditure/v1/updateExpenditure`,
      headers,
      data: {
        description,
        expenditureHeader,
        expenditureAmount,
        fileName,
        expenditureId,
        expenditureDate,
        updatedBy: {
          userId: user.userId,
        },
      },
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
         //alert(res.data.message);
          toast.success(res.data.message)
        } else if (res.data.responseCode === 400) {
          toast.error(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setExpenditureDate("");
    setDescription("");
    setExpenditureHeader("");
    setExpenditureAmount("");
    setFileName("");
    setOnEdit(false);
    setKey("list-view");
    FetchData();
  };

  return (
    <>
      <main id="main" className="main">
        <div className={key === "add" ? "pagetitle mb-5" : "pagetitle"}>
          <h1>Expenditure</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/Admin/masters">Masters</Link>
              </li>
              <li className="breadcrumb-item active">Expenditure</li>
            </ol>
          </nav>
        </div>
        <div className={key === "list-view" ? "displayForm mt-3" : "hideForm"}>
          <select
            value={value}
            onChange={handleChange}
            style={{ borderColor: "powderblue", borderRadius: 3, padding: 2 }}
          >
            <option value="10">10 / Pages</option>
            <option value="25">25 / Pages</option>
            <option value="50">50 / Pages</option>
            <option value="100">100 / Pages</option>
          </select>
        </div>
        <Card style={{ marginTop: 10 }}>
          <Card.Body>
            <Tabs
              activeKey={key}
              onSelect={(key) => setKey(key)}
              className="mt-3"
            >
              <Tab eventKey="list-view" title="List View">
                <Table striped bordered hover responsive>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>FileName</th>

                      <th>Expenditure Date</th>
                      <th>Description</th>
                      <th>ExpenditureHeader</th>
                      <th>ExpenditureAmount</th>
                      <th>Created By</th>
                      <th>Updated By</th>
                      <th>Inserted Date</th>
                      <th>Updated Date</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {expenditure.map((data, index) => {
                      return (
                        <tr key={data.expenditureId}>
                          <th scope="row">{index + 1}</th>
                          <td>{data.fileName}</td>

                          <td>{data.expenditureDate}</td>
                          <td
                            style={{
                              "max-width": "200px",
                              overflow: "hidden",
                              "text-overflow": "ellipsis",
                              height: "56px",
                            }}
                          >
                            {data.description}
                          </td>
                          <td>{data.expenditureHeader}</td>
                          <td>{data.expenditureAmount}</td>
                          <td>{data.createdBy.userName}</td>
                          <td>{data.updatedBy.userName}</td>
                          <td>
                            {Moment(data.insertedDate).format("DD-MM-YYYY")}
                          </td>
                          <td>
                            {Moment(data.updatedDate).format("DD-MM-YYYY")}
                          </td>
                          <td>
                            <button
                              type="button"
                              className="btn btn-icon btn-sm"
                              title="Edit"
                              onClick={(e) => handleEdit(data.expenditureId, e)}
                            //   onClick={console.log(data.expenditureId)}
                            >
                              <i class="bi bi-pencil-square"></i>
                            </button>
                            <button
                              type="button"
                              className="btn btn-icon btn-sm js-sweetalert"
                              title="Delete"
                              onClick={(e) => handleRemove(data.expenditureId, e)}
                            >
                              <i class="bi bi-trash"></i>
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
                <div>
                  <ReactPaginate
                    previousLabel="<"
                    nextLabel=">"
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination justify-content-center"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              </Tab>
              <Tab eventKey="add" title={onEdit ? "Edit" : "Add"}>
                <Form>
                <Form.Group className="mb-3 col-12">
                    <Form.Label>FileName</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add FileName"
                      value={fileName}
                      onChange={(e) => setFileName(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Expenditure Date</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Expenditure Date"
                      value={expenditureDate}
                      onChange={(e) => setExpenditureDate(e.target.value)}
                    />
                    {/* <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text> */}
                  </Form.Group>

                  <Form.Group className="mb-3 col-12">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add Description"
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-12">
                    <Form.Label>ExpenditureHeader</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add ExpenditureHeader"
                      value={expenditureHeader}
                      onChange={(e) => setExpenditureHeader(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3 col-12">
                    <Form.Label>Expenditure Amount</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add ExpenditureAmount"
                      value={expenditureAmount}
                      onChange={(e) => setExpenditureAmount(e.target.value)}
                    />
                  </Form.Group>
                  {/* <Form.Group className="mb-3 col-12">
                    <Form.Label>FileName</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add FileName"
                      value={fileName}
                      onChange={(e) => setFileName(e.target.value)}
                    />
                  </Form.Group> */}
                  {onEdit ? (
                    <Button
                      variant="primary"
                      onClick={(e) => editData(e)}
                      type="submit"
                    >
                      Edit
                    </Button>
                  ) : (
                    <Button
                      variant="primary"
                      onClick={(e) => postData(e)}
                      type="submit"
                    >
                      Submit
                    </Button>
                  )}
                  <Button
                    variant="outline-primary"
                    type="submit"
                    style={{ marginLeft: 15 }}
                    onClick={(e) => setKey(1)}
                  >
                    Cancel
                  </Button>
                </Form>
              </Tab>
            </Tabs>
          </Card.Body>
        </Card>
      </main>
    </>
  );
}
