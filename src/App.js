import "./App.css";
import React, { useState, useEffect } from "react";
import { Routes, Route, Navigate, useLocation } from "react-router-dom";
import Dashboard from "./Dashboard/Dashboard";
import LandLogin from "./LandLogin";
import Layout from "./components/Layout";
import Routings from "./Routings";

function RequireAuth({ children }) {
  const user = JSON.parse(sessionStorage.getItem("user"));
  let location = useLocation();
  if (!user) {
    return <Navigate to="/Admin/login" state={{ from: location }} replace />;
  } else if (user) {
  }
  return children;
}
function App() {
  const [authenticated, setAuthenticated] = useState(null);

  useEffect(() => {
    const user = JSON.parse(sessionStorage.getItem("user"));
    user ? setAuthenticated(true) : setAuthenticated(false);
  }, []);

  return (
    <Routes>
      {!authenticated && <Route path="/Admin/login" element={<LandLogin />} />}

      <Route
        path="/Admin"
        element={
          <RequireAuth>
            <Layout />
          </RequireAuth>
        }
      >
        <Route index element={<Dashboard />} />
        {Routings.map((item, index) => {
          return <Route key={index} path={item.path} element={item.element} />;
        })}
      </Route>

      <Route
        path="*"
        element={<Navigate to={authenticated ? "/Admin" : "/Admin/login"} />}
      />
    </Routes>
  );
}

export default App;
